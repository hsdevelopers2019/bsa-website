<?php 
namespace App\Laravel\Requests\Frontend;

use Session,Auth, Input;
use App\Laravel\Requests\RequestManager;

class ContactInquiryRequest extends RequestManager{

	public function rules(){

		$rules = [
			'name' => "required",
			'email' => "required|email",
			'phone' => "required|phone:PH,mobile",
			'message' => "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This field is required.",
		];
	}

	public function response(array $errors)
	{
		if ($this->ajax() || $this->wantsJson())
		{
			return new JsonResponse($errors, 422);
		}

		Session::flash('notification-status','failed');
		Session::flash('notification-msg','Some fields are not accepted.');

		return $this->redirector->to($this->getRedirectUrl()."#contact")
		->withInput($this->except($this->dontFlash))
		->withErrors($errors, $this->errorBag);
	}
}