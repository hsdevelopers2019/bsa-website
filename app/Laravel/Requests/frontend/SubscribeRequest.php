<?php 
namespace App\Laravel\Requests\Frontend;

use Session,Auth, Input;
use App\Laravel\Requests\RequestManager;

class SubscribeRequest extends RequestManager{

	public function rules(){

		$rules = [
			'email' => "required|email|unique:subscribers",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This field is required.",
		];
	}

	public function response(array $errors)
	{
		if ($this->ajax() || $this->wantsJson())
		{
			return new JsonResponse($errors, 422);
		}

		Session::flash('notification-status','failed');
		Session::flash('notification-msg','Some fields are not accepted.');

		return $this->redirector->to($this->getRedirectUrl()."#subscribe")
		->withInput($this->except($this->dontFlash))
		->withErrors($errors, $this->errorBag);
	}
}