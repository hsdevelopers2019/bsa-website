<?php

namespace App\Laravel\Requests\System;

use App\Laravel\Requests\RequestManager;
// use JWTAuth;

class FaqRequest extends RequestManager
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->user();
        $id = $this->route('id') ?: 0;
        
        $rules = [
            'question' => "required",
            'answer'   => 'required',
        ];

        return $rules;
    }

    public function messages() {

        return [
            'required'  => "Field is required.",
        ];
    }
}
