<?php

namespace App\Laravel\Notifications\Article;

use App\Laravel\Models\Article;

use App\Laravel\Notifications\MainNotification;
use Helper;

class NewCommentNotification extends MainNotification
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Article $article,$content)
    {
        $data = [
            'type' => "ARTICLE_COMMENT",
            'reference_id' => $article->id,
            'title' => "{$article->title}",
            'content'   => $content,
            'thumbnail' => $article->thumbnail,
        ];

        $this->setData($data);
    }
}