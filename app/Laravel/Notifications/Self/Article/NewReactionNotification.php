<?php

namespace App\Laravel\Notifications\Self\Article;

use App\Laravel\Models\User;
use App\Laravel\Models\Article;

use App\Laravel\Notifications\MainNotification;
use Helper;

class NewReactionNotification extends MainNotification
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user,Article $article)
    {
        $data = [
            'type' => "ARTICLE_REACTION",
            'reference_id' => $article->id,
            'title' => "{$article->title}",
            'content'   => "{$user->name} reacted on your article.", //Juan dela cruz reacted on your article.
            'thumbnail' => $article->thumbnail,
        ];

        $this->setData($data);
    }
}