<?php

namespace App\Laravel\Notifications\Announcement;

use App\Laravel\Models\User;
use App\Laravel\Models\Announcement;

use App\Laravel\Notifications\MainNotification;
use Helper;

class AnnouncementNotification extends MainNotification
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Announcement $announcement)
    {
        $data = [
            'type' => "ANNOUNCEMENT",
            'reference_id' => $announcement->id,
            'title' => "{$announcement->title}",
            'content' => "{$announcement->excerpt}",
            'thumbnail' => "",
        ];

        $this->setData($data);
    }
}