<?php

namespace App\Laravel\Notifications\Messages;

use Illuminate\Bus\Queueable;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;
use App\Laravel\Models\ChatConversation;
class BroadcastGroupChatMessage implements ShouldBroadcast
{
    use SerializesModels;

    public $message;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ChatConversation $message)
    {
        $this->message = $message;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel("groupchat.{$this->message->chat_id}");
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        $message = $this->message;
        $user = $message->sender;

        $msg_full_path = $msg_thumb_path = "";
        $user_full_path = $user_thumb_path = "";


        if($message->type == "image"){
            $msg_full_path = "{$message->directory}/resized/{$message->filename}";
            $msg_thumb_path = "{$message->directory}/thumbnails/{$message->filename}";
        }

        if($message->type == "file"){
            $msg_full_path = "{$message->directory}/{$message->filename}";
            $msg_thumb_path = "{$message->directory}/{$message->filename}";
        }

        if($user->filename){
            $user_full_path = "{$user->directory}/resized/{$user->filename}";
            $user_thumb_path = "{$user->directory}/thumbnails/{$user->filename}";
        }else{
            $user_full_path = $user_thumb_path = asset('placeholder/user.jpg');
        }

        return [
            'id' => $message->id ?:0,
            'sender_user_id' => $message->sender_user_id,
            'content' => $message->content,
            'type' => $message->type,
            'info'  => [
                'data' => [
                    'date_created' => [
                        'date_db' => $message->date_db($message->created_at,env("MASTER_DB_DRIVER","mysql")),
                        'month_year' => $message->month_year($message->created_at),
                        'time_passed' => $message->chat_time_passed($message->created_at),
                        'timestamp' => $message->created_at
                    ],
                    'attachment'  => [
                        'filename' => $message->filename,
                        'path' => $message->path,
                        'directory' => $message->directory,
                        'size' => $message->size,
                        'full_path' => $msg_full_path,
                        'thumb_path' => $msg_thumb_path,
                    ],
                ],
            ],
            'author'   => [
                'data' => [
                    'id' => $user->id ?:0,
                    'name' => $user->name,
                    'username' => $user->username,
                    'type' => $user->type,
                    'specialty_id' => $user->specialty_id,
                    'specialty' => $user->specialty?$user->specialty->name:"n/a",

                    'email' => $user->email,
                    'description' => $user->description,

                    'is_verified' => $user->is_verified,
                    'address1' => $user->address1,
                    'address2' => $user->address2,
                    'city' => $user->city,
                    'state' => $user->state,
                    'country_iso' => $user->country_iso,
                    'country_code' => $user->country_code,
                    'contact_number' => $user->contact_number,
                    'full_contact_number' => $user->country_code.$user->contact_number,
                    'info' => [
                        'data' => [
                            'member_since' => [
                                'date_db' => $user->date_db($user->created_at,env("MASTER_DB_DRIVER","mysql")),
                                'month_year' => $user->month_year($user->created_at),
                                'time_passed' => $user->time_passed($user->created_at),
                                'timestamp' => $user->created_at
                            ],
                            'last_activity' => [
                                'date_db' => $user->date_db($user->last_activity,env("MASTER_DB_DRIVER","mysql")),
                                'month_year' => $user->month_year($user->last_activity),
                                'time_passed' => $user->time_passed($user->last_activity),
                                'timestamp' => $user->last_activity
                            ],
                            'last_login' => [
                                'date_db' => $user->date_db($user->last_login,env("MASTER_DB_DRIVER","mysql")),
                                'month_year' => $user->month_year($user->last_login),
                                'time_passed' => $user->time_passed($user->last_login),
                                'timestamp' => $user->last_login
                            ],
                            'avatar' => [
                                'path' => $user->directory,
                                'filename' => $user->filename,
                                'path' => $user->path,
                                'directory' => $user->directory,
                                'full_path' => $user_full_path,
                                'thumb_path' => $user_thumb_path,
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * The event's broadcast name.
     *
     * @return string
     */
    public function broadcastAs()
    {
        return 'new_message';
    }
}
