<?php

$this->group([

	/**
	*
	* Backend routes main config
	*/
	'namespace' => "Frontend", 
	'as' => "frontend.", 
	// 'prefix'	=> "admin",
	// 'middleware' => "", 

	], function(){

		$this->get('/',['as' => "homepage",'uses' => "MainController@homepage"]);


		$this->get('about',['as' => "about", 'uses' => "MainController@about"]);

		$this->get('contact',['as' => "contact", 'uses' => "MainController@contact"]);

		$this->get('opportunities',['as'=> "opportunities",'uses' => "MainController@opportunities"]);

		$this->get('gallery',['as' => "gallery", 'uses' => "MainController@gallery"]);

		$this->get('service',['as' => "service", 'uses' => "MainController@service"]);

		$this->get('team',['as' => "team", 'uses' => "MainController@team"]);

		$this->get('blog',['as' => "blog", 'uses' => "MainController@blog"]);

		$this->get('blog/{id?}',['as' => "blog_details", 'uses' => "MainController@blog_details"]);

		$this->post('subscriber',['as' => 'subscribe', 'uses' => "MainController@subscribe"]);


		$this->post('create',['as' => 'save', 'uses' => "MainController@store_getInTouch"]);

});