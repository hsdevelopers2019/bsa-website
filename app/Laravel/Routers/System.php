<?php

$this->group([

	/**
	*
	* Backend routes main config
	*/
	'namespace' => "System", 
	'as' => "system.", 
	'prefix'	=> "admin",
	// 'middleware' => "", 

], function(){

	$this->group(['middleware' => ["web","system.guest"]], function(){
		$this->get('register/{_token?}',['as' => "register",'uses' => "AuthController@register"]);
		$this->post('register/{_token?}',['uses' => "AuthController@store"]);
		$this->get('login/{redirect_uri?}',['as' => "login",'uses' => "AuthController@login"]);
		$this->post('login/{redirect_uri?}',['uses' => "AuthController@authenticate"]);
	});



	$this->group(['middleware' => ["web","system.auth","system.client_partner_not_allowed"]], function(){
		
		$this->get('lock',['as' => "lock", 'uses' => "AuthController@lock"]);
		$this->post('lock',['uses' => "AuthController@unlock"]);
		$this->get('logout',['as' => "logout",'uses' => "AuthController@destroy"]);

		$this->group(['as' => "account."],function(){
			$this->get('p/{username?}',['as' => "profile",'uses' => "AccountController@profile"]);
			$this->group(['prefix' => "setting"],function(){
				$this->get('info',['as' => "edit-info",'uses' => "AccountController@edit_info"]);
				$this->post('info',['uses' => "AccountController@update_info"]);
				$this->get('password',['as' => "edit-password",'uses' => "AccountController@edit_password"]);
				$this->post('password',['uses' => "AccountController@update_password"]);
			});


			
		});

		$this->group(['middleware' => ["system.update_profile_first"]], function() {
			$this->get('/',['as' => "dashboard",'uses' => "DashboardController@index"]);

			$this->group(['prefix' => "system-account", 'as' => "user."], function () {
				$this->get('/',['as' => "index", 'uses' => "UserController@index"]);
				$this->get('inactive',['as' => "inactive", 'uses' => "UserController@inactive_user"]);
				
				$this->get('create',['as' => "create", 'uses' => "UserController@create"]);
				$this->post('create',['uses' => "UserController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "UserController@edit"]);

				$this->post('edit/{id?}',['uses' => "UserController@update"]);

				$this->any('activate/{id?}',['as'=>'activate','uses' => "UserController@activate"]);

				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "UserController@destroy"]);
			});

			$this->group(['prefix' => "file-manager", 'as' => "file."], function () {
				$this->get('/',['as' => "index", 'uses' => "FileManagerController@index"]);
				$this->get('create',['as' => "create", 'uses' => "FileManagerController@create"]);
				$this->post('create',['uses' => "FileManagerController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "FileManagerController@edit"]);
				$this->post('edit/{id?}',['uses' => "FileManagerController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "FileManagerController@destroy"]);
			});

			/*$this->group(['prefix' => "chat", 'as' => "chat."], function () {
				$this->get('/',['as' => "index", 'uses' => "ChatController@index"]);
				$this->get('show/{id?}',['as' => "show", 'uses' => "ChatController@show"]);
			});
*/
			/*$this->group(['prefix' => "mentorship", 'as' => "mentorship."], function () {
				$this->get('/',['as' => "index", 'uses' => "MentorshipController@index"]);
				$this->get('show/{id?}',['as' => "show", 'uses' => "MentorshipController@show"]);
			});*/

			$this->group(['prefix' => "page", 'as' => "page."], function () {
				$this->get('/',['as' => "index", 'uses' => "PageController@index"]);
				$this->get('create',['as' => "create", 'uses' => "PageController@create"]);
				$this->post('create',['uses' => "PageController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "PageController@edit"]);
				$this->post('edit/{id?}',['uses' => "PageController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "Pag	eController@destroy"]);
			});

			$this->group(['prefix' => "service", 'as' => "service."], function () {
				$this->get('/',['as' => "index", 'uses' => "ServiceController@index"]);
				$this->get('create',['as' => "create", 'uses' => "ServiceController@create"]);
				$this->post('create',['uses' => "ServiceController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "ServiceController@edit"]);
				$this->post('edit/{id?}',['uses' => "ServiceController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "ServiceController@destroy"]);
			});

			$this->group(['prefix' => "about", 'as' => "about."], function () {

				$this->get('/',['as' => "index", 'uses' => "AboutController@index"]);

				$this->get('create',['as' => "create", 'uses' => "AboutController@create"]);

				$this->post('create',['uses' => "AboutController@store"]);

				$this->get('edit/{id?}',['as' => "edit", 'uses' => "AboutController@edit"]);

				$this->post('update',['as'=>'update','uses' => "AboutController@update"]);

				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "AboutController@destroy"]);
			});


			$this->group(['prefix' => "opportunities", 'as' => "opportunity."], function () {

				$this->get('/',['as' => "index", 'uses' => "OpportunityController@index"]);

				$this->get('create',['as' => "create", 'uses' => "OpportunityController@create"]);

				$this->post('create',['uses' => "OpportunityController@store"]);

				$this->get('edit/{id?}',['as' => "edit", 'uses' => "OpportunityController@edit"]);

				$this->post('edit/{id?}',['uses' => "OpportunityController@update"]);

				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "OpportunityController@destroy"]);
			});

			$this->group(['prefix' => "contact", 'as' => "contact."], function () {

				$this->get('/',['as' => "index", 'uses' => "ContactController@index"]);

				$this->get('create',['as' => "create", 'uses' => "ContactController@create"]);

				$this->post('create',['uses' => "ContactController@store"]);

				$this->post('create-getintouch',['as'=>'save','uses' => "ContactController@store_getInTouch"]);

				$this->get('edit/{id?}',['as' => "edit", 'uses' => "ContactController@edit"]);

				$this->post('update',['as'=>'update','uses' => "ContactController@update"]);

				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "ContactController@destroy"]);
			});



			$this->group(['prefix' => "product", 'as' => "products."], function () {
				$this->get('/',['as' => "index", 'uses' => "ProductController@index"]);
				$this->get('create',['as' => "create", 'uses' => "ProductController@create"]);
				$this->post('create',['uses' => "ProductController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "ProductController@edit"]);
				$this->post('edit/{id?}',['uses' => "ProductController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "ProductController@destroy"]);
			});

			$this->group(['prefix' => "inquiries", 'as' => "inquiry."], function () {
				$this->get('/',['as' => "index", 'uses' => "ContactInquiryController@index"]);
			});

			$this->group(['prefix' => "category", 'as' => "category."], function () {
				$this->get('/',['as' => "index", 'uses' => "CategoryController@index"]);
				$this->get('create',['as' => "create", 'uses' => "CategoryController@create"]);
				$this->post('create',['uses' => "CategoryController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "CategoryController@edit"]);
				$this->post('edit/{id?}',['uses' => "CategoryController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "CategoryController@destroy"]);
			});

			$this->group(['prefix' => "article", 'as' => "article."], function () {
				$this->get('/',['as' => "index", 'uses' => "ArticleController@index"]);
				$this->get('published',['as' => "published", 'uses' => "ArticleController@published"]);
				$this->get('pending',['as' => "pending", 'uses' => "ArticleController@pending"]);

				$this->get('create',['as' => "create", 'uses' => "ArticleController@create"]);
				$this->post('create',['uses' => "ArticleController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "ArticleController@edit"]);
				$this->post('edit/{id?}',['uses' => "ArticleController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "ArticleController@destroy"]);
			});

			$this->group(['prefix' => "announcement", 'as' => "announcement."], function () {
				$this->get('/',['as' => "index", 'uses' => "AnnouncementController@index"]);
				$this->get('published',['as' => "published", 'uses' => "AnnouncementController@published"]);
				$this->get('pending',['as' => "pending", 'uses' => "AnnouncementController@pending"]);

				$this->get('create',['as' => "create", 'uses' => "AnnouncementController@create"]);
				$this->post('create',['uses' => "AnnouncementController@store"]);
				$this->get('re-notify/{id?}',['as' => "force_notification", 'uses' => "AnnouncementController@force_notification"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "AnnouncementController@edit"]);
				$this->post('edit/{id?}',['uses' => "AnnouncementController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "AnnouncementController@destroy"]);
			});

			$this->group(['prefix' => "general-settings", 'as' => "settings."], function () {
				$this->get('/',['as' => "index", 'uses' => "GeneralSettingsController@index"]);
				$this->post('/',['uses' => "GeneralSettingsController@store"]);
			});

			$this->group(['prefix' => "teams", 'as' => "team."], function () {
				$this->get('/',['as' => "index", 'uses' => "TeamController@index"]);
				$this->get('create',['as' => "create", 'uses' => "TeamController@create"]);
				$this->post('create',['uses' => "TeamController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "TeamController@edit"]);
				$this->post('edit/{id?}',['uses' => "TeamController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "TeamController@destroy"]);
			});

			$this->group(['prefix' => "news", 'as' => "news."], function () {
				$this->get('/',['as' => "index", 'uses' => "NewsController@index"]);
				$this->get('published',['as' => "published", 'uses' => "NewsController@published"]);
				$this->get('pending',['as' => "pending", 'uses' => "NewsController@pending"]);

				$this->get('create',['as' => "create", 'uses' => "NewsController@create"]);
				$this->post('create',['uses' => "NewsController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "NewsController@edit"]);
				$this->post('edit/{id?}',['uses' => "NewsController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "NewsController@destroy"]);
			});

			$this->group(['prefix' => "image-slider", 'as' => "image_slider."], function () {
				$this->get('/',['as' => "index", 'uses' => "ImageSliderController@index"]);

				$this->get('create',['as' => "create", 'uses' => "ImageSliderController@create"]);

				$this->post('create',['uses' => "ImageSliderController@store"]);

				$this->get('edit/{id?}',['as' => "edit", 'uses' => "ImageSliderController@edit"]);
				$this->post('edit/{id?}',['uses' => "ImageSliderController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "ImageSliderController@destroy"]);
			});

			$this->group(['prefix' => "subscribers", 'as' => "subscriber."], function () {
				$this->get('/',['as' => "index", 'uses' => "SubscriberController@index"]);
			});

			$this->group(['prefix' => "widget", 'as' => "widget."], function () {
				$this->get('/',['as' => "index", 'uses' => "WidgetController@index"]);
				$this->get('create',['as' => "create", 'uses' => "WidgetController@create"]);
				$this->post('create',['uses' => "WidgetController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "WidgetController@edit"]);
				$this->post('edit/{id?}',['uses' => "WidgetController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "WidgetController@destroy"]);
			});

			$this->group(['prefix' => "gallery", 'as' => "gallery."], function () {
				$this->get('/',['as' => "index", 'uses' => "GalleryController@index"]);
				$this->get('create',['as' => "create", 'uses' => "GalleryController@create"]);
				$this->post('create',['uses' => "GalleryController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "GalleryController@edit"]);
				$this->post('edit/{id?}',['uses' => "GalleryController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "GalleryController@destroy"]);
			});

			$this->group(['prefix' => "faqs", 'as' => "faq."], function () {
				$this->get('/',['as' => "index", 'uses' => "FaqController@index"]);
				$this->get('create',['as' => "create", 'uses' => "FaqController@create"]);
				$this->post('create',['uses' => "FaqController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "FaqController@edit"]);
				$this->post('edit/{id?}',['uses' => "FaqController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "FaqController@destroy"]);
			});

			$this->group(['prefix' => "testimonies", 'as' => "testimonial."], function () {
				$this->get('/',['as' => "index", 'uses' => "TestimonialController@index"]);
				$this->get('create',['as' => "create", 'uses' => "TestimonialController@create"]);
				$this->post('create',['uses' => "TestimonialController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "TestimonialController@edit"]);
				$this->post('edit/{id?}',['uses' => "TestimonialController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "TestimonialController@destroy"]);
			});
			$this->group(['prefix' => "get-in-touch", 'as' => "get-in-touch."], function () {
				$this->get('/',['as' => "index", 'uses' => "GetInTouchController@index"]);
				
			});
			/*$this->group(['prefix' => "specialty", 'as' => "specialty."], function () {
				$this->get('/',['as' => "index", 'uses' => "SpecialtyController@index"]);
				$this->get('create',['as' => "create", 'uses' => "SpecialtyController@create"]);
				$this->post('create',['uses' => "SpecialtyController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "SpecialtyController@edit"]);
				$this->post('edit/{id?}',['uses' => "SpecialtyController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "SpecialtyController@destroy"]);
			});*/

			/*$this->group(['prefix' => "app-user", 'as' => "app_user."], function () {
				$this->get('/',['as' => "index", 'uses' => "AppUserController@index"]);
				$this->get('export',['as' => "export", 'uses' => "AppUserController@export"]);

				$this->get('mentors',['as' => "mentor", 'uses' => "AppUserController@mentors"]);
				$this->get('mentees',['as' => "mentee", 'uses' => "AppUserController@mentees"]);

				$this->get('create',['as' => "create", 'uses' => "AppUserController@create"]);
				$this->post('create',['uses' => "AppUserController@store"]);
				
				$this->get('import',['as' => "import", 'uses' => "AppUserController@import"]);
				$this->post('import',['uses' => "AppUserController@submit_import"]);

				$this->get('edit/{id?}',['as' => "edit", 'uses' => "AppUserController@edit"]);
				$this->post('edit/{id?}',['uses' => "AppUserController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "AppUserController@destroy"]);
			});*/


			/*$this->group(['prefix' => "form", 'as' => "form."], function () {
				$this->get('/',['as' => "index", 'uses' => "FormController@index"]);
				$this->get('create',['as' => "create", 'uses' => "FormController@create"]);
				$this->post('create',['uses' => "FormController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "FormController@edit"]);
				$this->post('edit/{id?}',['uses' => "FormController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "FormController@destroy"]);
			});

			$this->get('ratings',['as' => "index", 'uses' => "RatingController@index"]);*/
		});
	});
});