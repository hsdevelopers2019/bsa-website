@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-8">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-success">
        <div class="panel-heading panel-heading-divider">Opportunities - Edit Record Form<span class="panel-subtitle">Opportunity information.</span></div>
        <div class="panel-body">
          <form method="POST" action="" enctype="multipart/form-data">
            
            {!!csrf_field()!!}
          
            
            <div class="form-group {{$errors->first('title') ? 'has-error' : NULL}}">
              <label>Opportunity Title</label>
              <input type="text" placeholder="Headline of the announcement" class="form-control" name="title" value="{{old('title',$service->title)}}">
              @if($errors->first('title'))
              <span class="help-block">{{$errors->first('title')}}</span>
              @endif
            </div>

            <div class="form-group {{$errors->first('description') ? 'has-error' : NULL}}">
              <label>Opportunity Details</label>
              <textarea name="description" cols="30" id="description" rows="10" class="form-control editor">{!!old('description',$service->description)!!}</textarea>
              @if($errors->first('description'))
              <span class="help-block">{{$errors->first('description')}}</span>
              @endif
            </div>

  
                                
            <div class="row xs-pt-15">
              <div class="col-xs-6">
                  <button type="submit" class="btn btn-space btn-success">Update Record</button>
                  <a href="{{route('system.opportunity.index')}}" class="btn btn-space btn-default">Cancel</a>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@stop




@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/ckeditor/contents.css')}}"/>
@stop

@section('page-scripts')
<script src="{{asset('assets/lib/ckeditor/ckeditor.js')}}" type="text/javascript"></script>

@stop