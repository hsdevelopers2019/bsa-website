@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-6">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-success">
        <div class="panel-heading panel-heading-divider">Update Record Form<span class="panel-subtitle">Team Information.</span></div>
        <div class="panel-body">
          <form method="POST" action="" enctype="multipart/form-data">
            {!!csrf_field()!!}
            
            <div class="form-group {{$errors->first('name') ? 'has-error' : NULL}}">
              <label>Name</label>
              <input type="text" placeholder="Name" class="form-control" name="name" value="{{old('name', $team->name)}}">
              @if($errors->first('name'))
              <span class="help-block">{{$errors->first('name')}}</span>
              @endif
            </div>

            <div class="form-group {{$errors->first('position') ? 'has-error' : NULL}}">
              <label>Position</label>
              <input type="text" placeholder="Position" class="form-control" name="position" value="{{old('position', $team->position)}}">
              @if($errors->first('position'))
              <span class="help-block">{{$errors->first('position')}}</span>
              @endif
            </div>

            <div class="form-group {{$errors->first('description') ? 'has-error' : NULL}}">
              <label>Description</label>
              <textarea class="form-control" name="description">{{ old('description', $team->description) }}</textarea>
              @if($errors->first('description'))
              <span class="help-block">{{$errors->first('description')}}</span>
              @endif
            </div>

            <div class="form-group {{$errors->first('file') ? 'has-error' : NULL}}">
              <label>Image</label><br>
              <img src="{{ "{$team->directory}/resized/{$team->filename}" }}" style="width: 150px; height: 150px;">
              <input type="file" placeholder="image" class="form-control" name="file">
              @if($errors->first('file'))
              <span class="help-block">{{$errors->first('file')}}</span>
              @endif
            </div>
            
            <div class="row xs-pt-15">
              <div class="col-xs-6">
                  <button type="submit" class="btn btn-space btn-success">Update Record</button>
                  <a href="{{route('system.team.index')}}" class="btn btn-space btn-default">Cancel</a>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@stop


