<div class="be-left-sidebar">
    <div class="left-sidebar-wrapper"><a href="#" class="left-sidebar-toggle">{{env('APP_NAME',"Human Capital Asia")}}</a>
        <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">
                <div class="left-sidebar-content">
                    <ul class="sidebar-elements">
                        <li class="divider">Quick Menu</li>
                        <li><a href="{{route('system.dashboard')}}"><i class="icon mdi mdi-home"></i><span>Dashboard &amp; Statistics</span></a> </li>
                        @if(in_array($auth->type, ["super_user","admin","editor","technical","chief_editor"]))
                        <li class="divider">Content Management</li>
                       {{--  <li class="parent"><a href="#"><i class="icon mdi mdi-attachment"></i><span>Articles</span></a>
                            <ul class="sub-menu">
                                <li><a href="{{route('system.article.pending')}}">Pending for Approval</a> </li>
                                <li><a href="{{route('system.article.published')}}">Published</a> </li>
                                <li><a href="{{route('system.article.index')}}">All records</a> </li>
                                <li><a href="{{route('system.article.create')}}">Create New</a> </li>
                            </ul>
                        </li> --}}

                       {{--  <li class="parent"><a href="#"><i class="icon mdi mdi-attachment"></i><span>News</span></a>
                            <ul class="sub-menu">
                                <li><a href="{{route('system.news.pending')}}">Pending for Approval</a> </li>
                                <li><a href="{{route('system.news.published')}}">Published</a> </li>
                                <li><a href="{{route('system.news.index')}}">All records</a> </li>
                                <li><a href="{{route('system.news.create')}}">Create New</a> </li>
                            </ul>
                        </li> --}}

                         <li class="parent"><a href="#"><i class="icon mdi mdi-settings-square"></i><span>Services</span></a>
                            <ul class="sub-menu">
                                <li><a href="{{route('system.service.index')}}">All records</a> </li>
                                <li><a href="{{route('system.service.create')}}">Create New</a> </li>
                            </ul>
                        </li>

                        
                        {{-- <li><a href="{{route('system.about.create')}}">About Create/Edit</a> </li> --}}
                        <li ><a href="{{route('system.about.create')}}"><i class="icon mdi mdi-info"></i><span>About us</span></a>
                            {{-- <ul class="sub-menu">
                                <li><a href="{{route('system.about.index')}}">All records</a> </li>
                                <li><a href="{{route('system.about.create')}}">About Create/Edit</a> </li>
                            </ul> --}}
                        </li>
                         <li>
                             <a href="{{route('system.contact.create')}}"> <i class="icon mdi mdi-phone"></i><span>Contact</span></a>
                         </li>
                        <li class="parent"><a href="#"><i class="icon mdi mdi-case-check"></i><span>Opportunity</span></a>
                            <ul class="sub-menu">
                                <li><a href="{{route('system.opportunity.index')}}">All records</a> </li>
                                <li><a href="{{route('system.opportunity.create')}}">Create New</a> </li>
                            </ul>
                        </li>

                      {{--   <li class="parent"><a href="#"><i class="icon mdi mdi-shopping-basket"></i><span>Products</span></a>
                            <ul class="sub-menu">
                                <li><a href="{{route('system.products.index')}}">All records</a> </li>
                                <li><a href="{{route('system.products.create')}}">Create New</a> </li>
                            </ul>
                        </li> --}}

                       {{--  <li class="parent"><a href="#"><i class="icon mdi mdi-people"></i><span>Teams</span></a>
                            <ul class="sub-menu">
                                <li><a href="{{route('system.team.index')}}">All records</a> </li>
                                <li><a href="{{route('system.team.create')}}">Create New</a> </li>
                            </ul>
                        </li> --}}
                        {{--  <li class="parent"><a href="#"><i class="icon mdi mdi-picture-in-picture"></i><span>Opportunity</span></a>
                            <ul class="sub-menu">
                                <li><a href="{{route('system.opportunity.index')}}">All records</a> </li>
                                <li><a href="{{route('system.opportunity.create')}}">Create New</a> </li>
                            </ul>
                        </li> --}}

                        <li class="parent"><a href="#"><i class="icon mdi mdi-picture-in-picture"></i><span>Image Sliders</span></a>
                            <ul class="sub-menu">
                                <li><a href="{{route('system.image_slider.index')}}">All records</a> </li>
                                <li><a href="{{route('system.image_slider.create')}}">Create New</a> </li>
                            </ul>
                        </li>

                        <li class="parent"><a href="#"><i class="icon mdi mdi-image"></i><span>Gallery</span></a>
                            <ul class="sub-menu">
                                <li><a href="{{route('system.gallery.index')}}">All records</a> </li>
                                <li><a href="{{route('system.gallery.create')}}">Create New</a> </li>
                            </ul>
                        </li>
                       <li><a href="{{ route('system.subscriber.index') }}"><i class="icon mdi mdi-face"></i><span>Subscriber</span></a>
                        <li><a href="{{ route('system.get-in-touch.index') }}"><i class="icon mdi mdi-face"></i><span>Get in Touch</span></a>
    
                       {{--  <li class="parent"><a href="#"><i class="icon mdi mdi-question-answer"></i><span>FAQ's</span></a>
                            <ul class="sub-menu">
                                <li><a href="{{route('system.faq.index')}}">All records</a> </li>
                                <li><a href="{{route('system.faq.create')}}">Create New</a> </li>
                            </ul>
                        </li> --}}

                       {{--  <li class="parent"><a href="#"><i class="icon mdi mdi-message-text"></i><span>Testimonies</span></a>
                            <ul class="sub-menu">
                                <li><a href="{{route('system.testimonial.index')}}">All records</a> </li>
                                <li><a href="{{route('system.testimonial.create')}}">Create New</a> </li>
                            </ul>
                        </li> --}}

                       {{--  <li><a href="{{route('system.inquiry.index')}}"><i class="icon mdi mdi-phone"></i><span>Contact Inquiries</span></a> </li> --}}

                        
                      {{--   <li><a href="{{route('system.subscriber.index')}}"><i class="icon mdi mdi-phone"></i><span>Subscribers</span></a> </li> --}}

                        @endif


                        @if(in_array($auth->type, ["super_user","admin"]))

                            <li class="divider">Account Management</li>
                           
                            <li class="parent"><a href="#"><i class="icon mdi mdi-face"></i><span>System Accounts</span></a>
                                <ul class="sub-menu">
                                    <li><a href="{{route('system.user.index')}}">All records</a> </li>
                                    <li><a href="{{route('system.user.create')}}">Add new account</a> </li>
                                </ul>
                            </li>
                        @endif
{{-- 
                        @if(in_array($auth->type, ["super_user","admin","technical","chief_editor"]))

                            <li class="divider">Mastefile</li>
                            <li><a href="{{route('system.settings.index')}}"><i class="icon mdi mdi-settings"></i><span>General Settings</span></a> </li>
                            <li><a href="{{route('system.page.index')}}"><i class="icon mdi mdi-assignment-o"></i><span>Pages</span></a></li>
                            <li><a href="{{route('system.widget.index')}}"><i class="icon mdi mdi-assignment-o"></i><span>Widget</span></a></li>

                            <li class="parent"><a href="#"><i class="icon mdi mdi-collection-folder-image"></i><span>File Manager</span></a>
                                <ul class="sub-menu">
                                    <li><a href="{{route('system.file.index')}}">All records</a> </li>
                                    <li><a href="{{route('system.file.create')}}">Create New</a> </li>
                                </ul>
                            </li>

                            @if(in_array($auth->type, ["super_user","admin","technical"]))
                                <li class="parent"><a href="#"><i class="icon mdi mdi-collection-text"></i><span>Categories</span></a>
                                    <ul class="sub-menu">
                                        <li><a href="{{route('system.category.index')}}">All records</a> </li>
                                        <li><a href="{{route('system.category.create')}}">Create New</a> </li>
                                    </ul>
                                </li>
                            @endif
                        @endif --}}
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>