<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="{{env("APP_NAME","Human  Capital Asia Backoffice")}}">
<meta name="author" content="Richard Kennedy Domingo">
<link rel="shortcut icon" href="{{asset('assets/logo.png')}}">
<title>{{$page_title}} :: {{env("APP_NAME","Human Capital Asia Back Office")}}</title>