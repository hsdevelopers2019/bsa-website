@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-6">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-success">
        <div class="panel-heading panel-heading-divider">Update Record Form<span class="panel-subtitle">Modify General Settings</span></div>
        <div class="panel-body">
          <form method="POST" action="" enctype="multipart/form-data">
            {!!csrf_field()!!}

            <div class="form-group {{$errors->first('recipient_email') ? 'has-error' : NULL}}">
             <input type="hidden" id="id" name="id" class="form-control" value="{{old('id', $setting->id ? : 0 )}}">
              <label>Recipient Email</label>
              <input type="email" placeholder="Recipient Email" class="form-control" name="recipient_email" value="{{old('recipient_email',$setting->recipient_email ? : '' )}}">
              @if($errors->first('recipient_email'))
              <span class="help-block">{{$errors->first('recipient_email')}}</span>
              @endif
            </div>
                  
            <div class="form-group {{$errors->first('email') ? 'has-error' : NULL}}">
             <input type="hidden" id="id" name="id" class="form-control" value="{{old('id', $setting->id ? : 0 )}}">
              <label>Email</label>
              <input type="email" placeholder="Email" class="form-control" name="email" value="{{old('email',$setting->email ? : '' )}}">
              @if($errors->first('email'))
              <span class="help-block">{{$errors->first('email')}}</span>
              @endif
            </div>

            <div class="form-group {{$errors->first('contact') ? 'has-error' : NULL}}">
              <label>Contact Number</label>
              <input type="text" placeholder="Contact" class="form-control" name="contact" value="{{old('contact',$setting->contact ? : '')}}">
              @if($errors->first('contact'))
              <span class="help-block">{{$errors->first('contact')}}</span>
              @endif
            </div>

            <div class="form-group {{$errors->first('fb_link') ? 'has-error' : NULL}}">
              <label>Facebook Link</label>
              <input type="text" placeholder="Facebook Link" class="form-control" name="fb_link" value="{{old('fb_link',$setting->fb_link ? : '')}}">
              @if($errors->first('fb_link'))
              <span class="help-block">{{$errors->first('fb_link')}}</span>
              @endif
            </div>

            <div class="form-group {{$errors->first('twitter_link') ? 'has-error' : NULL}}">
              <label>Twitter Link</label>
              <input type="text" placeholder="Twitter Link" class="form-control" name="twitter_link" value="{{old('twitter_link',$setting->twitter_link ? : '')}}">
              @if($errors->first('twitter_link'))
              <span class="help-block">{{$errors->first('twitter_link')}}</span>
              @endif
            </div>

             <div class="form-group {{$errors->first('instagram_link') ? 'has-error' : NULL}}">
              <label>Instagram Link</label>
              <input type="text" placeholder="Instagram Link" class="form-control" name="instagram_link" value="{{old('instagram_link',$setting->instagram_link ? : '')}}">
              @if($errors->first('instagram_link'))
              <span class="help-block">{{$errors->first('instagram_link')}}</span>
              @endif
            </div>

             <div class="form-group {{$errors->first('youtube_link') ? 'has-error' : NULL}}">
              <label>Youtube Link</label>
              <input type="text" placeholder="Youtube Link" class="form-control" name="youtube_link" value="{{old('youtube_link',$setting->youtube_link ? : '')}}">
              @if($errors->first('youtube_link'))
              <span class="help-block">{{$errors->first('youtube_link')}}</span>
              @endif
            </div>

            {{-- <div class="form-group {{$errors->first('content') ? 'has-error' : NULL}}">
              <label>Content</label>
              <textarea class="form-control" rows="4" name="content">{{ old('content', $setting->content) }}</textarea>
              @if($errors->first('content'))
              <span class="help-block">{{$errors->first('content')}}</span>
              @endif
            </div> --}}

            {{-- <div class="form-group {{$errors->first('company_address') ? 'has-error' : NULL}}">
              <label>Address</label>
              <input type="text" placeholder="Address" class="form-control" name="company_address" value="{{old('company_address',$setting->address ? : '' )}}">
              @if($errors->first('company_address'))
              <span class="help-block">{{$errors->first('company_address')}}</span>
              @endif
            </div> --}}

            <div class="form-group {{ $errors->has('geo_lat') ? "has-danger" : NULL }} row">
              <label class="col-md-2 label-control" for="geo_lat">Geo Latitude</label>
              <div class="col-md-9">
                <input type="text" id="geo_lat" class="form-control input-sm" placeholder="Geo Latitude" name="geo_lat" id="geo_lat" value="{{ old('geo_lat',$setting->geo_lat) }}">
                @if($errors->has('geo_lat')) <p class="text-xs-left"><small class="danger text-muted">{{ $errors->first('geo_lat') }}</small></p> @endif
              </div>
            </div>

            <div class="form-group {{ $errors->has('geo_long') ? "has-danger" : NULL }} row">
              <label class="col-md-2 label-control" for="geo_long">Geo Longitude</label>
              <div class="col-md-9">
                <input type="text" id="geo_long" class="form-control input-sm" placeholder="Geo Longitude" name="geo_long" id="geo_long" value="{{ old('geo_long',$setting->geo_long) }}">
                @if($errors->has('geo_long')) <p class="text-xs-left"><small class="danger text-muted">{{ $errors->first('geo_long') }}</small></p> @endif
              </div>
            </div>

            <div class="form-group {{ $errors->has('address') ? "has-danger" : NULL }} row">
              <label class="col-md-2 label-control" for="device_type">Geo-location</label>
              <div class="col-md-9">
                <div class="input-group">
                  <span class="input-group-addon"><i class="icon-search"></i></span>
                  <input type="text" id="map-address" class="form-control disableSubmitOnEnter input-sm" placeholder="Search Location" name="address">
                </div>
                <input type="hidden" name="street_address" id="street_address">
                <input type="hidden" name="city" id="city">
                <input type="hidden" name="state" id="state">
                <input type="hidden" name="country" id="country">
                <div id="map" style="width: 100%; height: 300px;"></div>
              </div>
              @if($errors->first('address'))
              <span class="help-block" style="color: red;">{{$errors->first('address')}}</span>
              @endif
            </div>

            <div class="row xs-pt-15">
              <div class="col-xs-6">
                  <button type="submit" class="btn btn-space btn-success">Update Record</button>
                  <a href="{{route('system.settings.index')}}" class="btn btn-space btn-default">Cancel</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
@section('page-styles')
@stop

@section('page-scripts')
<script src="http://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_API_KEY')}}"></script>
<script src="http://maps.google.com/maps/api/js?sensor=true&v=3&libraries=places&key={{env('GOOGLE_MAP_KEY')}}"></script>
<script type="text/javascript">     
    $(function(){           
        function updateControls(addressComponents) {
          $('#street_address').val(addressComponents.addressLine1);
          $('#city').val(addressComponents.city);
          $('#state').val(addressComponents.stateOrProvince);
          $('#country').val(addressComponents.country);
        }

        $('#map').locationpicker({
            location: {
                latitude: {{ old('geo_lat', $setting->geo_lat ?: 14.5995) }},
                longitude: {{ old('geo_long', $setting->geo_long ?: 120.9842) }}
            },
            radius: 50,
            inputBinding : {
              locationNameInput: $('#map-address'),
              latitudeInput: $('#geo_lat'),
              longitudeInput: $('#geo_long'),
            },
            enableAutocomplete: true,
            autocompleteOptions: {
                componentRestrictions: {country: 'ph'}
            },
            onchanged: function (currentLocation, radius, isMarkerDropped) {
                var addressComponents = $(this).locationpicker('map').location.addressComponents;
                updateControls(addressComponents);
            },
            oninitialized: function(component) {
                var addressComponents = $(component).locationpicker('map').location.addressComponents;
                updateControls(addressComponents);
            }
        });    
    }); 
</script>
@stop

