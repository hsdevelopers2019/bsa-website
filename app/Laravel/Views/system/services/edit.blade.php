@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-8">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-success">
        <div class="panel-heading panel-heading-divider">Update Record Form<span class="panel-subtitle">Services information.</span></div>
        <div class="panel-body">
          <form method="POST" action="" enctype="multipart/form-data">
            {!!csrf_field()!!}
            @if($service->filename)
            <div class="form-group">
              <label for="">Current Thumbnail</label>
              <img src="{{"{$service->directory}/resized/$service->filename"}}" alt="" class="img-thumbnail">
            </div>
            @endif
            
            <div class="form-group {{$errors->first('service_title') ? 'has-error' : NULL}}">
              <label>Service Title</label>
              <input type="text" placeholder="Headline of the announcement" class="form-control" name="service_title" value="{{old('service_title',$service->service_title)}}">
              @if($errors->first('service_title'))
              <span class="help-block">{{$errors->first('service_title')}}</span>
              @endif
            </div>

            <div class="form-group {{$errors->first('content') ? 'has-error' : NULL}}">
              <label>Service Details</label>
              <textarea name="content" cols="30" id="content" rows="10" class="form-control editor">{!!old('content',$service->content)!!}</textarea>
              @if($errors->first('content'))
              <span class="help-block">{{$errors->first('content')}}</span>
              @endif
            </div>

            <div class="form-group {{$errors->first('file') ? 'has-error' : NULL}}">
              <label>Thumbnail</label>
              <input type="file"  class="form-control form-file" name="file" value="{{old('file')}}">
              @if($errors->first('file'))
              <span class="help-block">{{$errors->first('file')}}</span>
              @endif
            </div>
                                
            <div class="row xs-pt-15">
              <div class="col-xs-6">
                  <button type="submit" class="btn btn-space btn-success">Update Record</button>
                  <a href="{{route('system.service.index')}}" class="btn btn-space btn-default">Cancel</a>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@stop




@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/ckeditor/contents.css')}}"/>
@stop

@section('page-scripts')
<script src="{{asset('assets/lib/ckeditor/ckeditor.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){
    CKEDITOR.replace( 'content' );

  })
</script>
@stop