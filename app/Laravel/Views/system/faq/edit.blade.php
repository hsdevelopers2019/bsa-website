@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-6">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-success">
        <div class="panel-heading panel-heading-divider">Create Record Form<span class="panel-subtitle">FAQ Information.</span></div>
        <div class="panel-body">
          <form method="POST" action="">
            {!!csrf_field()!!}
            
            <div class="form-group {{$errors->first('question') ? 'has-error' : NULL}}">
              <label>Question</label>
              <input type="text" placeholder="Question" class="form-control" name="question" value="{{old('question', $faq->question)}}">
              @if($errors->first('question'))
              <span class="help-block">{{$errors->first('question')}}</span>
              @endif
            </div>

            <div class="form-group {{$errors->first('answer') ? 'has-error' : NULL}}">
              <label>Answer</label>
              <textarea class="form-control" rows="5" name="answer">{{ old('answer', $faq->answer) }}</textarea>
              @if($errors->first('answer'))
              <span class="help-block">{{$errors->first('answer')}}</span>
              @endif
            </div>
            
            <div class="row xs-pt-15">
              <div class="col-xs-6">
                  <button type="submit" class="btn btn-space btn-success">Edit Record</button>
                  <a href="{{route('system.faq.index')}}" class="btn btn-space btn-default">Cancel</a>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@stop


