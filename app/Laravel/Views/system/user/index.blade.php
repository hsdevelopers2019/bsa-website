@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-12">
      @include('system._components.notifications')
      <div class="panel panel-default panel-table  panel-border-color panel-border-color-success">
        <div class="panel-heading">Active User -Record Data
          <div class="tools dropdown">
            <a href="#" type="button" data-toggle="dropdown" class="dropdown-toggle"><span class="icon mdi mdi-more-vert"></span></a>
            <ul role="menu" class="dropdown-menu pull-right">
              <li><a href="{{route('system.user.create')}}">Add new account</a></li>
              <li><a href="{{route('system.user.inactive')}}">View Inactive User</a></li>
              {{-- <li><a href="#">Import Calendar</a></li> --}}
              {{-- <li class="divider"></li> --}}
              {{-- <li><a href="#">Export calendar</a></li> --}}
            </ul>
          </div>
        </div>
        <div class="panel-body">
          <table class="table table-hover table-wrapper">
            <thead>
              <tr>
                <th >Account Name</th>
                <th>Email</th>
                <th>Username</th>
                <th>Date Added</th>
                <th>Account Type</th>
                <th class="actions"></th>
              </tr>
            </thead>
            <tbody>
              @forelse($users as $user)
              <tr>

                <td class="user-avatar"> <img src="{{$user->avatar}}" alt="{{$user->name}}">{{$user->name}}</td>
                <td class="cell-detail"> 
                  <span><a href="mailto:{{$user->email}}">{{$user->email}}</a></span>
                  <span class="cell-detail-description">{{"@".$user->username}}</span>
                </td>
                <td class="cell-detail"> 
                  <span class="">{{$user->username}}</span>
                </td>
                <td>{{Helper::date_only($user->created_at)}}</td>
                <td>{{$user->type}}</td>
          
                  <td class="actions">
                    <div class="btn-group btn-hspace">
                      <button type="button"  data-toggle="dropdown" class="btn btn-lg btn-default dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings"></i> Actions <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                      <ul role="menu" class="dropdown-menu">
                        <li><center><a  style="text-decoration: none;color:#000" href="{{route('system.user.edit',[$user->id])}}" title="Edit Record">Edit</a></center></li>
                        <li class="divider"></li>
                        <li> <a href="#" class="btn btn-sm action-delete" data-url="{{route('system.user.destroy',[$user->id])}}" data-toggle="modal" data-target="#confirm-delete" title="Deactivate user">Deactivate User</a></li>
                      </ul>
                    </div>
                </td>
             
              </tr>
              @empty
              <td colspan="5" class="text-center"><i>No record found yet.</i> <a href="{{route('system.user.create')}}"><strong>Click here</strong></a> to create one.</td>
              @endforelse
            </tbody>
          </table>
          
        </div>
        <div class="panel-footer">
          <div class="pagination-wrapper text-center">
            {!!$users->render()!!}
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
@stop

@section('page-modals')
<div id="confirm-delete" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Confirm your action</h1>
      </div>

      <div class="modal-body">
        <div role="alert" class="alert alert-warning alert-icon alert-icon-border alert-dismissible">
          <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
          <div class="message">
            <strong>Warning!</strong> This action can not be undone.
          </div>
        </div>
        <h3 class="text-semibold">Are you sure ...</h3>
        <p>You are about to Deactivate User, would you like to proceed? <strong>Or you just want to save it as draft?</strong></p>

        <hr>

        <h3 class="text-semibold">What is this message?</h3>
        <p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command or upon submission of your task of the day.</p>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
        <a href="#" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Deactivating user ..." class="btn btn-primary btn-raised btn-loading" id="btn-confirm-delete">Deactivate</a>
      </div>
    </div>
  </div>
</div>


@stop

@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){
    $('.datepicker').datetimepicker({autoclose: true})
    $(".action-delete").on("click",function(){
      var btn = $(this);
      $("#btn-confirm-delete").attr({"href" : btn.data('url')});
    });

    $('#btn-confirm-delete').on('click', function() {
      $('.btn-link').hide();
          $('.btn-loading').button('loading');
          $('#target').submit();
     });

    $('.table-wrapper').find(".btn-group").children("button").addClass("btn-xs");

  });
</script>
@stop

