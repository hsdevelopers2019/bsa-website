@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-6">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-success">
        <div class="panel-heading panel-heading-divider">Create Record Form<span class="panel-subtitle">Testimonial Information.</span></div>
        <div class="panel-body">
          <form method="POST" action="">
            {!!csrf_field()!!}
            
            <div class="form-group {{$errors->first('name') ? 'has-error' : NULL}}">
              <label>Name</label>
              <input type="text" placeholder="Name" class="form-control" name="name" value="{{old('name', $testimonial->name)}}">
              @if($errors->first('name'))
              <span class="help-block">{{$errors->first('name')}}</span>
              @endif
            </div>

            <div class="form-group {{$errors->first('position') ? 'has-error' : NULL}}">
              <label>Position</label>
              <input type="text" placeholder="Position" class="form-control" name="position" value="{{old('position', $testimonial->position)}}">
              @if($errors->first('position'))
              <span class="help-block">{{$errors->first('position')}}</span>
              @endif
            </div>

            <div class="form-group {{$errors->first('testimony') ? 'has-error' : NULL}}">
              <label>Testimony</label>
              <textarea class="form-control" rows="5" name="testimony">{{ old('testimony', $testimonial->testimony) }}</textarea>
              @if($errors->first('testimony'))
              <span class="help-block">{{$errors->first('testimony')}}</span>
              @endif
            </div>
            
            <div class="row xs-pt-15">
              <div class="col-xs-6">
                  <button type="submit" class="btn btn-space btn-success">Edit Record</button>
                  <a href="{{route('system.testimonial.index')}}" class="btn btn-space btn-default">Cancel</a>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@stop


