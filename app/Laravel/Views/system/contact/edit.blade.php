@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-6">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-success">
        <div class="panel-heading panel-heading-divider">Update Contact Record Form<span class="panel-subtitle">Contact Information</span></div>
        <div class="panel-body">
          <form method="POST" action="" enctype="multipart/form-data">

            {!!csrf_field()!!}
            
              {{-- <div class="form-group {{$errors->first('description') ? 'has-error' : NULL}}">
              <label>Description</label>
              <textarea class="form-control" rows="5" name="description">{{ $edit_contacts->description }}</textarea>
              @if($errors->first('description'))
              <span class="help-block">{{$errors->first('description')}}</span>
              @endif
            </div> --}}
            <div class="form-group {{$errors->first('description') ? 'has-error' : NULL}} xs-pt-10">
                <label class="control-label">Contact desciption</label> 
                <textarea name="description" id="content" cols="30" rows="10" class="form-control summernote" placeholder="Enter your content">{!! old('description',$contact->description) !!}</textarea>
                @if($errors->first('content'))
                <span class="help-block">{!!$errors->first('description')!!}</span>
                @endif
            </div>

            <div class="form-group {{$errors->first('phone') ? 'has-error' : NULL}}">
              <label>Phone number</label>
              <input type="text" placeholder="phone" class="form-control" name="phone" value="{{old('phone',$edit_contacts->phone)}}">
              @if($errors->first('phone'))
              <span class="help-block">{{$errors->first('phone')}}</span>
              @endif
            </div>

            <div class="form-group {{$errors->first('email') ? 'has-error' : NULL}}">
              <label>email</label>
              <input type="email"  class="form-control" name="email" value="{{old('email',$edit_contacts->email)}}" >
              @if($errors->first('email'))
              <span class="help-block">{{$errors->first('email')}}</span>
              @endif
            </div>
            <div class="form-group {{$errors->first('address') ? 'has-error' : NULL}}">
              <label>Address</label>
              <input type="address"  class="form-control" name="address" value="{{old('address',$edit_contacts->address)}}" >
              @if($errors->first('address'))
              <span class="help-block">{{$errors->first('address')}}</span>
              @endif
            </div>
            <div class="row xs-pt-15">
              <div class="col-xs-6">
                  <button type="submit" class="btn btn-space btn-success">Update Record</button>
                  <a href="{{route('system.contact.index')}}" class="btn btn-space btn-default">Cancel</a>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/ckeditor/contents.css')}}"/>
@stop

@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/ckeditor/ckeditor.js')}}" type="text/javascript"></script>

<script type="text/javascript">
    $(function(){
    CKEDITOR.replace( 'content' );
  })
 
</script>
@stop

