@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-8  ">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-success">
        <div class="panel-heading panel-heading-divider">Update/Create Contact Information<span class="panel-subtitle">Contact  Information</span></div>
        
        <div class="panel-body">
        

           @forelse($contacts as $contact)
              <form method="POST" action="{{ route('system.contact.update') }}" enctype="multipart/form-data">

            {!!csrf_field()!!}
            <input type="text" name="id"  style="display: none" value="{{ $contact->id }}" >
              {{-- <div class="form-group {{$errors->first('description') ? 'has-error' : NULL}}">
              <label>Description</label>
              <textarea class="form-control" rows="5" name="description">{{ $contact->description }}</textarea>
              @if($errors->first('description'))
              <span class="help-block">{{$errors->first('description')}}</span>
              @endif
            </div> --}}

            <div class="form-group {{$errors->first('description') ? 'has-error' : NULL}} xs-pt-10">
                <label class="control-label">Service Details</label> 
                <textarea name="description" id="content" cols="30" rows="10" class="form-control summernote" placeholder="Enter your content">{!! old('description',$contact->description) !!}</textarea>
                @if($errors->first('content'))
                <span class="help-block">{!!$errors->first('description')!!}</span>
                
                @endif
            </div>

            <div class="form-group {{$errors->first('phone') ? 'has-error' : NULL}}">
              <label>Phone number</label>
              <input type="text" placeholder="phone" class="form-control" name="phone" value="{{old('phone',$contact->phone)}}">
              @if($errors->first('phone'))
              <span class="help-block">{{$errors->first('phone')}}</span>
              @endif
            </div>

            <div class="form-group {{$errors->first('email') ? 'has-error' : NULL}}">
              <label>email</label>
              <input type="email"  class="form-control" name="email" value="{{old('email',$contact->email)}}" >
              @if($errors->first('email'))
              <span class="help-block">{{$errors->first('email')}}</span>
              @endif
            </div>
            <div class="form-group {{$errors->first('address') ? 'has-error' : NULL}}">
              <label>Address</label>
              <input type="address"  class="form-control" name="address" value="{{old('address',$contact->address)}}" >
              @if($errors->first('address'))
              <span class="help-block">{{$errors->first('address')}}</span>
              @endif
            </div>
            <div class="row xs-pt-15">
              <div class="col-xs-6">
                  <button type="submit" class="btn btn-space btn-success">Update Record</button>
                  <a href="{{route('system.contact.create')}}" class="btn btn-space btn-default">Cancel</a>
              </div>
            </div>

          </form>
            @empty
               <form method="POST" action="" enctype="multipart/form-data">

            {!!csrf_field()!!}
            
             {{--  <div class="form-group {{$errors->first('description') ? 'has-error' : NULL}}">
              <label>Description</label>
              <textarea class="form-control" rows="5" name="description"></textarea>
              @if($errors->first('description'))
              <span class="help-block">{{$errors->first('description')}}</span>
              @endif
            </div> --}}
             <div class="form-group {{$errors->first('description') ? 'has-error' : NULL}} xs-pt-10">
                <label class="control-label">Service Details</label> 
                <textarea name="description" id="content" cols="30" rows="10" class="form-control summernote" placeholder="Enter your content">{!! old('description') !!}</textarea>
                @if($errors->first('content'))
                <span class="help-block">{!!$errors->first('description')!!}</span>
                @endif
            </div>


            <div class="form-group {{$errors->first('phone') ? 'has-error' : NULL}}">
              <label>Phone number</label>
              <input type="text" placeholder="phone" class="form-control" name="phone" value="{{old('phone')}}">
              @if($errors->first('phone'))
              <span class="help-block">{{$errors->first('phone')}}</span>
              @endif
            </div>

            <div class="form-group {{$errors->first('email') ? 'has-error' : NULL}}">
              <label>email</label>
              <input type="email"  class="form-control" name="email" value="{{old('email')}}" >
              @if($errors->first('email'))
              <span class="help-block">{{$errors->first('email')}}</span>
              @endif
            </div>
            <div class="form-group {{$errors->first('address') ? 'has-error' : NULL}}">
              <label>Address</label>
              <input type="address"  class="form-control" name="address" value="{{old('address')}}" >
              @if($errors->first('address'))
              <span class="help-block">{{$errors->first('address')}}</span>
              @endif
            </div>
            <div class="row xs-pt-15">
              <div class="col-xs-6">
                  <button type="submit" class="btn btn-space btn-success">Create Record</button>
                  <a href="{{route('system.contact.create')}}" class="btn btn-space btn-default">Cancel</a>
              </div>
            </div>

          </form>
            @endforelse

           
         
         
        </div>
      </div>
    </div>
  </div>
</div>
@stop
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/ckeditor/contents.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/ckeditor/ckeditor.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){
    CKEDITOR.replace( 'content' );
  })
</script>
@stop
