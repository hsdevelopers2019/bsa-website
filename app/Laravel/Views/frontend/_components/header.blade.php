
  <div id="loading" class="theme-bg" style="background-color: #0B7600">
    <div class="lds-ripple"><div></div><div></div></div>
  </div>

<header>
    <nav class="navbar header-nav navbar-expand-lg">
      <div class="container">
        <!-- Brand -->
        <a class="navbar-brand m-30px-r" href="index.html">
          <img class="light-logo" src="{{ asset('frontend/assets/img/logo.png')}}" title="" alt="" style="width: 100px; height: 100px">
          <img class="dark-logo" src="{{ asset('frontend/assets/img/logo.png')}}" title="" alt="" style="width: 100px;height: 100px">
        </a>
        <!-- / -->

        <!-- Mobile Toggle -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
            <span></span>
            <span></span>
            <span></span>
        </button>
        <!-- / -->

        <!-- Top Menu -->
        <div class="collapse navbar-collapse justify-content-end" id="navbar">
          <ul class="navbar-nav ml-auto align-items-lg-center">
              <li><a class="nav-link" href="#home">Home</a></li>
            <li><a class="nav-link" href="#about">News</a></li>
             <li><a class="nav-link" href="#events">Events</a></li>
            <li><a class="nav-link" href="#team">Members</a></li>
            <li><a class="nav-link" href="contact.html">About</a></li>
          </ul>
        </div>
        <!-- / -->

      </div><!-- Container -->
    </nav> <!-- Navbar -->
  </header>

