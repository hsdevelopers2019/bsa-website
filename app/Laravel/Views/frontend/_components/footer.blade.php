<footer class="footer" style="background-color: #0B7600">
    <div class="footer-top">
      <div class="container">
        <div class="row">
          <div class="col-md-12 md-m-25px-b col-lg-4">
            <p class="m-25px-b"><a href="#"><img src="{{ asset('frontend/assets/img/logo.png')}}" title="" alt="" style="width: 100px; height 100px"></a></p>
            <ul class="social-icons">
              <li><a class="facebook" href="#"><i class="fab fa-facebook-square"></i></a></li>
              <li><a class="twitter" href="#"><i class="fab fa-twitter"></i></a></li>
              <li><a class="google" href="#"><i class="fab fa-linkedin"></i></a></li>
              <li><a class="linkedin" href="#"><i class="fab fa-instagram"></i></a></li>
            </ul>
          </div> <!-- col -->

          <div class="col-sm-6 col-md-3 col-lg-2 md-m-15px-tb">
            <h6>Company</h6>
            <ul class="nav flex-column">
              <li class="nav-item">
                <a href="#">About</a>  
              </li>
              <li class="nav-item">
                <a href="#">Contact</a>  
              </li>
                <li class="nav-item">
                <a href="#">Careers</a>  
              </li>
            </ul>
          </div> <!-- col -->

       
          <div class="col-sm-6 col-md-3 col-lg-2 md-m-15px-tb">
            <h6>Support</h6>
            <ul class="nav flex-column">
              <li class="nav-item">
                <a href="#">Help Center</a>  
              </li>
            </ul>
          </div> <!-- col -->

          <div class="col-sm-6 col-md-3 col-lg-3 md-m-15px-tb">
            <h6>Subscribe</h6>
            <div class="newsletter-box">
                <input name="email" placeholder="Email Address" class="form-control" type="text">
                <button type="submit" class="btn btn-theme"><i class="fab fa-telegram-plane"></i></button>
            </div>
          </div><!-- col -->

        </div>
      </div>
    </div> <!-- footer top -->

    <div class="footer-bottom">
      <div class="container">
        <div class="copyright text-center">
          <p>All © Copyright by . All Rights Reserved.</p>
        </div>
      </div>
    </div>
  </footer>
    <!--FOOER AREA END-->

   