{{--     
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/font-awesome.min.css')}}"  />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/flaticon-set.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/magnific-popup.css')}}"  />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/owl.carousel.min.css')}}"  />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/owl.theme.default.min.css')}}"  />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/animate.css')}}"  />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/bootsnav.css')}}"  />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/style.css')}}" >
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/responsive.css')}}" />

    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,800" rel="stylesheet">
 --}}

 <link href="{{ asset('frontend/assets/plugin/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/plugin/font-awesome/css/fontawesome-all.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/plugin/et-line/style.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/plugin/themify-icons/themify-icons.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/plugin/owl-carousel/css/owl.carousel.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/plugin/magnific/magnific-popup.css') }}" rel="stylesheet">
  <!-- End -->
  

  <!-- Theme css -->
  <link href="{{ asset('frontend/assets/css/style.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/css/common.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/css/color/default.css') }}" rel="stylesheet" id="color_theme">

<link rel="stylesheet" type="text/css" href="">

{{-- 
 <link rel="shortcut icon" type="image/ico" href="{{asset('frontend/assets/img/logo_icon.png')}}" />

    <!--====== STYLESHEETS ======-->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="{{ asset('frontend/assets/css/plugins.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/assets/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/assets/css/theme.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/assets/css/icons.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/assets/css/plugins/modal-video.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/assets/css/plugins/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/assets/css/plugins/normalize.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/assets/css/plugins/venobox.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/assets/css/plugins/owl.carousel.css') }}" rel="stylesheet"> --}}