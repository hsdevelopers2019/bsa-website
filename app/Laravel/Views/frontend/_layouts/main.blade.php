<!DOCTYPE html>
<html lang="en">

<head>
    @include('frontend._components.metas')
    @include('frontend._includes.styles')
</head>
{{-- <body>
	@include('frontend._components.loader')
    @include('frontend._components.header')
    @yield('content')
    @include('frontend._components.footer')
    @include('frontend._includes.scripts')
</body> --}}


<body>
	@include('frontend._components.loader')
    @include('frontend._components.header')
    @yield('content')
    @include('frontend._components.footer')
    @include('frontend._includes.scripts')
</body>






</html>
