 @extends('frontend._layouts.main')

@section('content')
 

<body data-spy="scroll" data-target="#navbar" data-offset="98">

  <section id="home" class="home-banner-03 section-overlay bg-cover bg-center-center" style="background-image: url({{ asset('frontend/assets/img/Ortigas-Skyline.jpeg')}})">

      <div class="overlay opacity7 dark-bg"></div>
      <div class="container">
        <div class="row full-screen align-items-center justify-content-center">
          <div class="col-lg-2"></div>
          <div class="col-md-10 col-lg-8 p-80px-tb">
            <div class="banner-text">
              <h3 class="white-color">I Want to: <span></span></h3>
              <input class="form-control ng-pristine ng-valid ng-touched" type="text" placeholder="Search">
              <div class="btn-bar d-flex justify-content-end">
                <a _ngcontent-c22="" class="text-white c-pointer" href="/content/i-want-to">View all</a>
              </div>
            </div>
          </div> <!-- col -->
           <div class="col-lg-2"></div>


        </div> <!-- row -->
        
      </div> <!-- container -->

    </section>

    <section id="events" class="section">
      <div class="container">

        <div class="row">
          <div class="col-md-6 col-lg-4 m-15px-tb">
            <div class="card">
              <div class="card-header bg-success" style="color: white">Captain's Corner</div>
              <div class="card-body">
                <div class="img">
                  <a href="#">  <img src="{{ asset('frontend/assets/img/blog-1.jpg')}}" title="" alt=""></a>
                </div>
                <div class="info">
                <div class="meta text-center" style="color: black ;font-size: 15px" ><a class="dark-color font-alt" href="#">Highlights New York Fashion Week 2018</a></div>
                <div class="text-center d-flex justify-content-between">
                <p> Lorem </p>
                <p>| Lorem </p>
                <p>| Lorem </p>
                <p>| Lorem</p>
                </div>
                  <div class="row">
                    <div class="col-lg-4">
                      <img src="{{ asset('frontend/assets/img/blog-1.jpg')}}" title="" alt="">
                    </div>
                    <div class="col-lg-4">
                      <img src="{{ asset('frontend/assets/img/blog-1.jpg')}}" title="" alt="">
                    </div>
                    <div class="col-lg-4">
                      <img src="{{ asset('frontend/assets/img/blog-1.jpg')}}"title="" alt="">
                    </div>
                  
                </div>
              </div>
              </div>
            </div>

          </div> <!-- col -->

          <div class="col-md-6 col-lg-4 m-15px-tb">
            <div class="card">
              <div class="card-header bg-success" style="color: white">Barangay San Antonio News</div>
                <div class="card-body" style="height: 370px; overflow-y: scroll;">
                  <div class="row">
                    <div class="col-lg-12">
                        <div class="card" style="border: none !important;">
                          <div class="row">
                            <div class="col-lg-3" >
                              <img src="{{ asset('frontend/assets/img/logo.png')}}" >
                            </div>
                            <div class="col-lg-9">
                              <a href="#">Makati Offers College Scholarships In Public And Private Universities, Colleges In Metro Manila</a>
                              <p style="font-size: 10px">September 23, 2019</p>
                              <p style="font-size: 14px">Makati offers college scholarships in public and private universities, Colleges in Metro Manila   Makati is now offering college scholarships of up to P80,000 for public high school </p>
                              <a class="d-flex justify-content-end" href="">Read more</a>
                              <hr>
                            </div>
                            <div class="col-lg-3" >
                              <img src="{{ asset('frontend/assets/img/logo.png')}}" >
                            </div>
                            <div class="col-lg-9">
                              <a href="#">Makati Offers College Scholarships In Public And Private Universities, Colleges In Metro Manila</a>
                              <p style="font-size: 10px">September 23, 2019</p>
                              <p style="font-size: 14px">Makati offers college scholarships in public and private universities, Colleges in Metro Manila   Makati is now offering college scholarships of up to P80,000 for public high school </p>
                              <a class="d-flex justify-content-end" href="">Read more</a>
                              <hr>
                            </div>
                          </div>                                                
                      </div>
                  </div>
                </div>
              </div> <!-- col -->
        </div> <!-- row -->
      </div>
      <div class="col-md-6 col-lg-4 m-15px-tb">
            <div class="card">
              <div class="card-header bg-success" style="color: white">Barangay San Antonio Events</div>
                <div class="card-body" style="height: 370px; overflow-y: scroll;">
                  <div class="row">
                    <div class="col-lg-12">
                        <div class="card" style="border: none !important;">
                          <div class="row">
                        

                          </div>                                                
                      </div>
                  </div>
                </div>
              </div> <!-- col -->
        </div> <!-- row -->
      </div>
    </section>
    <section id="team" class="section gray-bg">
      <div class="container">
        <div class="row justify-content-center m-45px-b sm-m-25px-b">
          <div class="col-lg-6 text-center">
            <div class="col-title">
              <h2 class="dark-color">Baranggay LGUs</h2>
              <div class="das-effect m-20px-tb"><span class="theme-bg"></span><span class="theme-bg"></span></div>
            </div>
          </div>
        </div> <!-- title row -->
        
        <div class="row">
          <div class="col-md-6 col-lg-4 m-15px-tb">
            <div class="our-team-01">
              <div class="team-img">
                <img src="{{ asset('frontend/assets/img/bsateam/kapt_trl.jpg')}}" title="" alt="">
                <ul class="team-social">
                  <li><a class="theme-bg" href="#"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a class="theme-bg" href="#"><i class="fab fa-twitter"></i></a></li>
                  <li><a class="theme-bg" href="#"><i class="fab fa-google-plus-g"></i></a></li>
                  <li><a class="theme-bg" href="#"><i class="fab fa-linkedin-in"></i></a></li>
                </ul>
              </div>
              <div class="team-info gray-bg">
                <h5 class="font-alt dark-color">Thomas Raymond Lising</h5>
                <label>Barangay Captain</label>
              </div>
            </div> <!-- team -->
          </div> <!-- col -->


           <div class="col-md-6 col-lg-4 m-15px-tb">
            <div class="our-team-01">
              <div class="team-img">
                <img src="{{ asset('frontend/assets/img/bsateam/demos.jpg')}}" title="" alt="">
                <ul class="team-social">
                  <li><a class="theme-bg" href="#"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a class="theme-bg" href="#"><i class="fab fa-twitter"></i></a></li>
                  <li><a class="theme-bg" href="#"><i class="fab fa-google-plus-g"></i></a></li>
                  <li><a class="theme-bg" href="#"><i class="fab fa-linkedin-in"></i></a></li>
                </ul>
              </div>
              <div class="team-info gray-bg">
                <h5 class="font-alt dark-color">Arjay F. Demos</h5>
                <label>SK Chairman</label>
              </div>
            </div> <!-- team -->
          </div> <!-- col -->

             <div class="col-md-6 col-lg-4 m-15px-tb">
            <div class="our-team-01">
              <div class="team-img">
                <img src="{{ asset('frontend/assets/img/bsateam/la_putt.jpg')}}" title="" alt="">
                <ul class="team-social">
                  <li><a class="theme-bg" href="#"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a class="theme-bg" href="#"><i class="fab fa-twitter"></i></a></li>
                  <li><a class="theme-bg" href="#"><i class="fab fa-google-plus-g"></i></a></li>
                  <li><a class="theme-bg" href="#"><i class="fab fa-linkedin-in"></i></a></li>
                </ul>
              </div>
              <div class="team-info gray-bg">
                <h5 class="font-alt dark-color">Venus T. La Putt</h5>
                <label>Barangay Kagawad</label>
              </div>
            </div> <!-- team -->
          </div> <!-- col -->

            

             <div class="col-md-6 col-lg-4 m-15px-tb">
            <div class="our-team-01">
              <div class="team-img">
                <img src="{{ asset('frontend/assets/img/bsateam/galang.jpg')}}" title="" alt="">
                <ul class="team-social">
                  <li><a class="theme-bg" href="#"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a class="theme-bg" href="#"><i class="fab fa-twitter"></i></a></li>
                  <li><a class="theme-bg" href="#"><i class="fab fa-google-plus-g"></i></a></li>
                  <li><a class="theme-bg" href="#"><i class="fab fa-linkedin-in"></i></a></li>
                </ul>
              </div>
              <div class="team-info gray-bg">
                <h5 class="font-alt dark-color">Justin Lanz R. Galang</h5>
                <label>Barangay Kagawad</label>
              </div>
            </div> <!-- team -->
          </div> <!-- col -->
          
            

             <div class="col-md-6 col-lg-4 m-15px-tb">
            <div class="our-team-01">
              <div class="team-img">
                <img src="{{ asset('frontend/assets/img/bsateam/kag_ivan.jpg')}}" title="" alt="">
                <ul class="team-social">
                  <li><a class="theme-bg" href="#"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a class="theme-bg" href="#"><i class="fab fa-twitter"></i></a></li>
                  <li><a class="theme-bg" href="#"><i class="fab fa-google-plus-g"></i></a></li>
                  <li><a class="theme-bg" href="#"><i class="fab fa-linkedin-in"></i></a></li>
                </ul>
              </div>
              <div class="team-info gray-bg">
                <h5 class="font-alt dark-color">Evaristo P. Rupisan</h5>
                <label>Barangay Kagawad</label>
              </div>
            </div> <!-- team -->
          </div> <!-- col -->




          <div class="col-md-6 col-lg-4 m-15px-tb">
            <div class="our-team-01">
              <div class="team-img">
                <img src="{{ asset('frontend/assets/img/bsateam/kag_pauleen.jpg')}}" title="" alt="">
                <ul class="team-social">
                  <li><a class="theme-bg" href="#"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a class="theme-bg" href="#"><i class="fab fa-twitter"></i></a></li>
                  <li><a class="theme-bg" href="#"><i class="fab fa-google-plus-g"></i></a></li>
                  <li><a class="theme-bg" href="#"><i class="fab fa-linkedin-in"></i></a></li>
                </ul>
              </div>
              <div class="team-info gray-bg">
                <h5 class="font-alt dark-color">Ma Pauline L. Blando</h5>
                <label>Barangay Kagawad</label>
              </div>
            </div> <!-- team -->
          </div> <!-- col -->


          <div class="col-md-6 col-lg-4 m-15px-tb">
            <div class="our-team-01">
              <div class="team-img">
                <img src="{{ asset('frontend/assets/img/bsateam/kag_sistoso.jpg')}}" title="" alt="">
                <ul class="team-social">
                  <li><a class="theme-bg" href="#"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a class="theme-bg" href="#"><i class="fab fa-twitter"></i></a></li>
                  <li><a class="theme-bg" href="#"><i class="fab fa-google-plus-g"></i></a></li>
                  <li><a class="theme-bg" href="#"><i class="fab fa-linkedin-in"></i></a></li>
                </ul>
              </div>
              <div class="team-info gray-bg">
                <h5 class="font-alt dark-color">Restituto T. Sistoso Jr.</h5>
                <label>Barangay Kagawad</label>
              </div>
            </div> <!-- team -->
          </div> <!-- col -->



          <div class="col-md-6 col-lg-4 m-15px-tb">
            <div class="our-team-01">
              <div class="team-img">
                <img src="{{ asset('frontend/assets/img/bsateam/medalla.jpg')}}" title="" alt="">
                <ul class="team-social">
                  <li><a class="theme-bg" href="#"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a class="theme-bg" href="#"><i class="fab fa-twitter"></i></a></li>
                  <li><a class="theme-bg" href="#"><i class="fab fa-google-plus-g"></i></a></li>
                  <li><a class="theme-bg" href="#"><i class="fab fa-linkedin-in"></i></a></li>
                </ul>
              </div>
              <div class="team-info gray-bg">
                <h5 class="font-alt dark-color">Normita S. Medalla</h5>
                <label>Barangay Kagawad</label>
              </div>
            </div> <!-- team -->
          </div> <!-- col -->



          <div class="col-md-6 col-lg-4 m-15px-tb">
            <div class="our-team-01">
              <div class="team-img">
                <img src="{{ asset('frontend/assets/img/bsateam/rustia.jpg')}}" title="" alt="">
                <ul class="team-social">
                  <li><a class="theme-bg" href="#"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a class="theme-bg" href="#"><i class="fab fa-twitter"></i></a></li>
                  <li><a class="theme-bg" href="#"><i class="fab fa-google-plus-g"></i></a></li>
                  <li><a class="theme-bg" href="#"><i class="fab fa-linkedin-in"></i></a></li>
                </ul>
              </div>
              <div class="team-info gray-bg">
                <h5 class="font-alt dark-color">Rachel Marie S. Rustia</h5>
                <label>Barangay Kagawad</label>
              </div>
            </div> <!-- team -->
          </div> <!-- col -->



        </div> <!-- row -->
      </div>
    </section>



    <!--
    ======================
    Services
    ======================
    -->
    <section id="about" class="section gray-bg">
      
      <div class="container">
          <div class="row justify-content-center m-45px-b sm-m-25px-b">
            <div class="col-lg-6 text-center">
              <div class="col-title">
                <h2 class="dark-color" >About Us</h2>
                <div class="das-effect m-20px-tb"><span class="theme-bg"></span><span class="theme-bg"></span></div>
                <p>The Baranggay is home of the Ortigas Center Business District, Phillipine Stock Exchange
              , University of Asia & the Pacific, Discovery Suites, Astoria Plaza, & Various high-rise Condominiums & Office Buildings.</p>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-4 col-md-6 m-15px-tb">
              <div class="feature-box-08">
                <span class="icon">
                  <i class="icon-target theme-color"></i>
                </span>
                <div class="feature-content">
                  <h6 class="dark-color">Our Mission</h6>
                  <p>Barangay San Antonio is committed to bring about a community of people united in purpose and equally involved in achieving its goals and objectives and in delivering basic services for the common good of its people.</p>
                </div>
                <a class="link-overlay" href="#"></a>
              </div> <!-- feature bx -->
            </div>                       

            <div class="col-lg-4 col-md-6 m-15px-tb">
              <div class="feature-box-08">
                <span class="icon">
                  <i class="icon-tools-2 theme-color"></i>
                </span>
                <div class="feature-content">
                  <h6 class="dark-color">Vision</h6>
                  <p>Barangay San Antonio brings poise and elegance to people by serving.</p>
                </div>
                <a class="link-overlay" href="#"></a>
              </div> <!-- feature bx -->
            </div> 

            <div class="col-lg-4 col-md-6 m-15px-tb">
              <div class="feature-box-08">
                <span class="icon">
                  <i class="icon-lightbulb theme-color"></i>
                </span>
                <div class="feature-content">
                  <h6 class="dark-color">Virtues</h6>
                  <p><b>TRUTH</b> – We uphold honesty and sincerity, consistency and integrity in thoughts, words and action.<br>
                  <b>PEACE</b> – Wholeness and fullness, being in harmony with all people and with all of creation.<br>
                <b>JUSTICE</b> – Right relationship with God, with oneself, with others (society) and with creation as a whole.</p>
                </div>
                <a class="link-overlay" href="#"></a>
              </div> <!-- feature bx -->
            </div> 
          </div> <!-- row -->
      </div> <!-- container -->
    </section>
 @endsection
