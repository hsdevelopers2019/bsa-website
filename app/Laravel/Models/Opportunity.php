<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;

class Opportunity extends Model
{
    protected $fillable = ['title','description'];
}
