<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Team extends Model
{
    use DateFormatterTrait,SoftDeletes;

	protected $table = "teams";
       
    protected $fillable = [
       'name' , 'position', 'description'
        
    ];

    protected $hidden = [];

 }
