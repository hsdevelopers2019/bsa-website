<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gallery extends Model
{
    use DateFormatterTrait,SoftDeletes;

	protected $table = "gallery";
       
    protected $fillable = [
        
    ];

    protected $hidden = [];
 }
