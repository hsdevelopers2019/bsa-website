<?php

namespace App\Laravel\Models;

use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IOSVersionControl extends Model
{
    use SoftDeletes, DateFormatterTrait;

    protected $table = "ios_version_control";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'version_name', 'major_version', 'minor_version', 
        'is_maintenance', 'maintenance_counter', 'changelogs'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'major_version' => "int",
        'minor_version' => "int",
        'maintenance_counter' => "int",
    ];
}
