<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class MentorshipParticipant extends Model
{
    use DateFormatterTrait,SoftDeletes;

	protected $table = "mentorship_participant";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    public $timestamps = true;


    public function author(){
        return $this->hasOne("App\Laravel\Models\User", "id", "user_id");
    }

    public function mentorship(){
        return $this->belongsTo("App\Laravel\Models\Mentorship", "mentorship_id", "id");
    }
}
