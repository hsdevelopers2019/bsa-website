<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mentorship extends Model
{
    use DateFormatterTrait,SoftDeletes;

	protected $table = "mentorship_header";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    public $timestamps = true;


    public function author(){
        return $this->hasOne('App\Laravel\Models\User','id','owner_user_id');
    }

    public function conversation(){
        return $this->hasMany('App\Laravel\Models\MentorshipConversation','mentorship_id','id');
    }

    public function latest_message(){
        return $this->hasOne('App\Laravel\Models\MentorshipConversation','mentorship_id','id')->where('type','<>','announcement')->orderBy('created_at','DESC');
    }

    public function participant(){
        return $this->hasMany('App\Laravel\Models\MentorshipParticipant','mentorship_id','id');
    }

    public function mentor(){
        return $this->belongsTo('App\Laravel\Models\User','mentor_user_id','id');
    }

    public function mentee(){
        return $this->belongsTo('App\Laravel\Models\User','mentee_user_id','id');
    }

    public function scopeKeyword($query, $keyword = NULL){
        if($keyword){
            $keyword = strtolower($keyword);
            return $query->whereRaw("LOWER(title) LIKE '{$keyword}%'");
        }
    }
}
