<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Products extends Model
{
    use DateFormatterTrait,SoftDeletes;
 
	protected $table = "products";

       
    protected $fillable = [
       'product_title' , 'content', 'excerpt'
        
    ];

    protected $hidden = [];

 }
