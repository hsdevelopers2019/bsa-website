<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable =['description','email','address','phone'];
}
