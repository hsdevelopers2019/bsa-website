<?php

namespace App\Laravel\Models;

use Carbon, Helper;
use App\Laravel\Models\Views\UserGroup;
use Illuminate\Notifications\Notifiable;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Contacts extends Authenticatable
{
 
protected $table = "contacts";


       
    protected $fillable = [
       'email' , 'address' , 'contact','status',
        
    ];

    protected $hidden = [];

 }
