<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class About extends Model
{
    use DateFormatterTrait,SoftDeletes;

	protected $table = "abouts";
       
    protected $fillable = [
       'title' , 'description'
        
    ];

    protected $hidden = [];

 }
