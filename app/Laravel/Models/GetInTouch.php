<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;

class GetInTouch extends Model
{
    protected $fillable = ['name','email','subject','message'];
}
