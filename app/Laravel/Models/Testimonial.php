<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Testimonial extends Model
{
    use DateFormatterTrait,SoftDeletes;

	protected $table = "testimonial";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'position', 'testimony'
    ];

    public $timestamps = true;

}
