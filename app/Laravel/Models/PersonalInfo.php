<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth,Helper;
class PersonalInfo extends Model
{
    use DateFormatterTrait,SoftDeletes;

	protected $table = "personal_info";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','name_id','birthday','age','gender','email','municipality','province','region','homeaddress','businessaddress','preffered_schedule','businessname'];

    public $timestamps = true;

}
