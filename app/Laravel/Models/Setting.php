<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Setting extends Model
{
    use DateFormatterTrait,SoftDeletes;

	protected $table = "settings";
       
    protected $fillable = [
       'recipient_email', 'email', 'contact', 'address', 'geo_lat', 'geo_long', 'fb_link', 'twitter_link', 'instagram_link', 'youtube_link'
        
    ];

    protected $hidden = [];

 }
