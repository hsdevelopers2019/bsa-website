<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;

class ContactInquiry extends Model
{
    use DateFormatterTrait;

	protected $table = "inquiries";
       
    protected $fillable = [
       'name' , 'subject', 'email', 'contact', 'message'
        
    ];

    protected $hidden = [];

 }
