<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class ImageSlider extends Model
{
    use DateFormatterTrait,SoftDeletes;

	protected $table = "image_sliders";
       
    protected $fillable = [
       'title' , 'description'
        
    ];

    protected $hidden = [];

 }
