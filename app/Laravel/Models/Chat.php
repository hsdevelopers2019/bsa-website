<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Chat extends Model
{
    use DateFormatterTrait,SoftDeletes;

	protected $table = "chat_header";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    protected $appends = ['thumbnail','new_directory'];

    public $timestamps = true;

    public function getNewDirectoryAttribute(){
        return str_replace(env("BLOB_STORAGE_URL"), env("CDN_STORAGE_URL"), $this->directory);
    }

    public function getThumbnailAttribute(){
        $full_path = asset('placeholder/group.jpg');
        
        if($this->filename){
            $full_path = "{$this->new_directory}/resized/{$this->filename}";
        }

        return $full_path;
    }


    public function author(){
        return $this->hasOne('App\Laravel\Models\User','id','owner_user_id');
    }

    public function conversation(){
        return $this->hasMany('App\Laravel\Models\ChatConversation','chat_id','id');
    }

    public function latest_message(){
        return $this->hasOne('App\Laravel\Models\ChatConversation','chat_id','id')->where('type','<>','announcement')->orderBy('created_at','DESC');
    }

    public function participant(){
        return $this->hasMany('App\Laravel\Models\ChatParticipant','chat_id','id')->withTrashed();
    }

    public function scopeKeyword($query, $keyword = NULL){
        if($keyword){
            $keyword = strtolower($keyword);
            return $query->whereRaw("LOWER(title) LIKE '{$keyword}%'");
        }
    }
}
