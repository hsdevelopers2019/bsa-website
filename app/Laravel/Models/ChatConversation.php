<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChatConversation extends Model
{
    use DateFormatterTrait,SoftDeletes;

	protected $table = "chat_conversation";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'content'
    ];

    protected $appends = ['new_directory'];

    public $timestamps = true;

    public function getNewDirectoryAttribute(){
        return str_replace(env("BLOB_STORAGE_URL"), env("CDN_STORAGE_URL"), $this->directory);
    }

    public function sender(){
        return $this->belongsTo("App\Laravel\Models\User", "sender_user_id", "id")->withTrashed();
    }

    public function chat(){
        return $this->belongsTo("App\Laravel\Models\Chat", "chat_id", "id");
    }

    public function participant(){
        return $this->belongsTo("App\Laravel\Models\ChatParticipant", "sender_user_id", "user_id")->where('chat_id',$this->chat_id)->withTrashed();
    }
}
