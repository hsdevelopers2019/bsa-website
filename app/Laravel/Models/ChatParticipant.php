<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChatParticipant extends Model
{
    use DateFormatterTrait,SoftDeletes;

	protected $table = "chat_participant";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    public $timestamps = true;


    public function author(){
        return $this->hasOne("App\Laravel\Models\User", "id", "user_id")->withTrashed();
    }

    public function chat(){
        return $this->belongsTo("App\Laravel\Models\Chat", "chat_id", "id")->withTrashed();
    }
}
