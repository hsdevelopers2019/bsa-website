<?php 

namespace App\Laravel\Controllers\Frontend;

/*
*
* Models used for this controller
*/
use App\Laravel\Models\ContactInquiry;
use App\Laravel\Models\Team;
use App\Laravel\Models\Article;
use App\Laravel\Models\Setting;
use App\Laravel\Models\Service;
use App\Laravel\Models\Gallery;
use App\Laravel\Models\ImageSlider;
use App\Laravel\Models\ArticleCategory as Category;
use App\Laravel\Models\Page;
use App\Laravel\Models\Subscriber;
use App\Laravel\Models\Testimonial;
use App\Laravel\Models\Faq;
use App\Laravel\Models\About;
use App\Laravel\Models\Opportunity;
use App\Laravel\Models\Contact;
use App\Laravel\Models\GetInTouch;

/*
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Frontend\ContactInquiryRequest;
use App\Laravel\Requests\Frontend\SubscribeRequest;
use App\Laravel\Requests\System\GetInTouchRequest;
use App\Laravel\Events\SendEmail;


/*
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, DB,Input,Event;

class MainController extends Controller{

	/*
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());

		$this->data['setting'] = Setting::first() ?: new Setting();
		$this->data['about'] = Page::where('id', 1)->first();
		$this->data['services'] = Service::all();
		$this->data['recent_blogs'] = Article::where('is_approved','yes')->orderBy('created_at', 'DESC')->take(2)->get();
		$this->data['contacts'] = Contact::orderBy('created_at', 'DESC')->take(1)->get();
	}

	public function index(){
		return $this->homepage();
	}

	public function homepage(){
		$this->data['sliders'] = ImageSlider::all();

		$this->data['galleries'] = Gallery::orderBy('created_at', 'DESC')->get();

		return view('frontend.page.home',$this->data);
	}

	public function about(){
		$this->data['about'] = "This is about page";
		
		$this->data['abouts'] = About::orderBy('created_at', 'DESC')->take(1)->get();
		
		return view('frontend.page.about', $this->data);
	}

	public function contact(){

		// $this->data['contacts'] = Contact::orderBy('created_at','DESC')->take(1)->get(); 
		return view('frontend.page.contact', $this->data);
	}

	public function gallery(){
		$this->data['galleries'] = Gallery::orderBy('created_at', 'DESC')->get();
		return view('frontend.page.gallery', $this->data);
	}

	public function service(){
		
		$this->data['services'] = Service::orderBy('created_at', 'ASC')->paginate(1);
		

		return view('frontend.page.service', $this->data);
	}

	public function team(){
		$this->data['teams'] = Team::all();
		return view('frontend.page.team', $this->data);
	}

	public function blog(){
		$this->data['blogs'] = Article::where('is_approved','yes')->orderBy('created_at', 'DESC')->paginate(3);
		return view('frontend.page.blog', $this->data);
	}

	public function blog_details($id = NULL){
		$article = Article::find($id);

		if($article) {
			$this->data['blog'] = Article::find($id);
			$this->data['previous'] = Article::where('id', '<', $id)->where('is_approved','yes')->orderBy('id', 'DESC')->first();
			$this->data['next'] = Article::where('id', '>', $id)->where('is_approved','yes')->orderBy('id', 'ASC')->first();
			$this->data['gallery'] = Gallery::inRandomOrder()->take(6)->get();
			$this->data['recents'] = Article::where('id', '<>', $id)->orderBy('created_at', 'DESC')->take(3)->get();
			$this->data['categories'] = Category::all();
			return view('frontend.page.blog-details', $this->data);
		}

		return redirect()->route('frontend.homepage');
	}


	public function opportunities(){

		$this->data['page_title'] = 'afdaf';
		$this->data['opportunities'] = Opportunity::orderBy('created_at','DESC')->paginate(2);
		return view('frontend.page.opportunities',$this->data);
	}

	public function subscribe(SubscribeRequest $request){
		// dd($request->all());
		$new_subscriber = new Subscriber;

		$new_subscriber->fill($request->all());

		if($new_subscriber->save()){
			session()->flash('notification-status','successs');
	 		session()->flash('notification-msg',"You successfully subscribe to our newsletter.");

			return redirect(url()->previous() . '#subscribe');
		} else {
			session()->flash('notification-status','warning');
	 		session()->flash('notification-msg',"Your attempt to subscribe to our newsletter was not successful.");

			return redirect(url()->previous() . '#subscribe');
		}
	}

	public function store_getInTouch(GetInTouchRequest $request){

		try {
			// dd($request->all());
			$new_contacts = new GetInTouch;
			$new_contacts->fill($request->all());
			

			if($new_contacts->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"Message has been sent Successfully");
				return redirect()->route('frontend.contact');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}


	}


}