<?php 

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controllerf
*/
// use App\Laravel\Models\ArticleCategory as Category;
use App\Laravel\Models\Subscriber;


/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\CategoryRequest;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str;

class SubscriberController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['heading'] = "Subscribers";
	}

	public function index () {
		$this->data['page_title'] = " :: Subscribers - Record Data";
		$this->data['subscribers'] = Subscriber::orderBy('created_at',"DESC")->paginate(15);
		return view('system.subscriber.index',$this->data);
	}
}