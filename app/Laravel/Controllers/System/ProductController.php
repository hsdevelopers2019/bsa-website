<?php namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Products;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\ProductRequest;
use App\Laravel\Requests\System\EditProductRequest;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str,ImageUploader;

class ProductController extends Controller
{
    protected $data;


    public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	
		$this->data['heading'] = "Products";
	}

	public function index () {
		$this->data['page_title'] = " :: Products - Record Data";
		$this->data['products'] = Products::orderBy('created_at',"DESC")->get();
		return view('system.products.index',$this->data);
	}

	public function create () {
		$this->data['page_title'] = " :: Products - Add Product";
		return view('system.products.create',$this->data);
	}

	public function store(ProductRequest $request){

		try {
			//dd($request->all());
			$new_products = new Products;
			$new_products->fill($request->all());
			$new_products->excerpt = Helper::get_excerpt($request->get('content'));
			
			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'uploads/images/products');
				$new_products->path = $upload["path"];
				$new_products->directory = $upload["directory"];
				$new_products->filename = $upload["filename"];
			}

			if($new_products->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New Product has been added.");
				return redirect()->route('system.products.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}


	}

	public function edit ($id = NULL) {

			$this->data['page_title'] = " :: Products - Edit record";
			$product = Products::find($id);

			if (!$product) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return view('system.products.index',$this->data);
			}

			$this->data['product'] = $product;
			return view('system.products.edit',$this->data);
	}

	public function update (ProductRequest $request, $id = NULL) {
		
			try {
			$products = Products::find($id);

			if (!$products) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.products.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to update special record.");
				return redirect()->route('system.products.index');	
			}
			$user = $request->user();
        	$products->fill($request->only('product_title','content'));
        	$products->excerpt = Helper::get_excerpt($request->get('content'));

        	if($request->hasFile('file')) {
        	    $image = ImageUploader::upload($request->file('file'), "uploads/images/products");
        	    $products->path = $image['path'];
        	    $products->directory = $image['directory'];
        	    $products->filename = $image['filename'];
            	
        	}

			// dispatch(new NotifyUsers($owner, $announcement));

			if($products->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.products.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$products = Products::find($id);


			if (!$products) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.products.index');
			}

			if($products->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.products.index');
			}

				session()->flash('notification-status','failed');
				session()->flash('notification-msg','Something went wrong.');

			} catch (Exception $e) {
				session()->flash('notification-status','failed');
				session()->flash('notification-msg',$e->getMessage());
				return redirect()->back();
			}
	}

}
