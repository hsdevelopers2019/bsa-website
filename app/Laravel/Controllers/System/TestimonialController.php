<?php 

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Testimonial;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\TestimonialRequest;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str;

class TestimonialController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	}

	public function index () {
		$this->data['page_title'] = " :: Testimonies - Record Data";
		$this->data['testimonials'] = Testimonial::orderBy('updated_at',"DESC")->paginate(15);
		return view('system.testimonial.index',$this->data);
	}

	public function create () {
		$this->data['page_title'] = " :: Testimonies - Add new record";
		return view('system.testimonial.create',$this->data);
	}

	public function store (TestimonialRequest $request) {
		try {
			$new_testimonial = new Testimonial;

			$new_testimonial->fill($request->only('name', 'position', 'testimony'));
			
			if($new_testimonial->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.testimonial.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$this->data['page_title'] = " :: Testimonies - Edit record";

		$testimonial = Testimonial::find($id);

		if (!$testimonial) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.testimonial.index');
		}

		if($id < 0){
			session()->flash('notification-status',"warning");
			session()->flash('notification-msg',"Unable to update special record.");
			return redirect()->route('system.testimonial.index');	
		}

		$this->data['testimonial'] = $testimonial;
		return view('system.testimonial.edit',$this->data);
	}

	public function update (TestimonialRequest $request, $id = NULL) {
		try {
			$testimonial = Testimonial::find($id);

			if (!$testimonial) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.testimonial.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to update special record.");
				return redirect()->route('system.testimonial.index');	
			}

			$testimonial->fill($request->only('name', 'position', 'testimony'));

			if($testimonial->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.testimonial.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$testimonial = Testimonial::find($id);

			if (!$testimonial) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.testimonial.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to remove special record.");
				return redirect()->route('system.testimonial.index');	
			}

			if($testimonial->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.testimonial.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}