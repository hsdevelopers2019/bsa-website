<?php namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/

use App\Laravel\Models\About;
use App\Laravel\Models\Opportunity;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\OpportunityRequest;


/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str,ImageUploader;

class OpportunityController extends Controller
{


	protected $data;


    public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	
		$this->data['heading'] = "Opportunity";
	}

	public function index () {
		$this->data['page_title'] = "Opportunity";
		$this->data['opportunities'] = Opportunity::orderBy('created_at',"DESC")->get();
	
		return view('system.opportunity.index',$this->data);
	}

	public function create () {
		$this->data['page_title'] = "Opportunity - Add Record";

		$this->data['description'] = About::all();
		return view('system.opportunity.create',$this->data);
	}

	public function store (OpportunityRequest $request) {
		try {
			// dd($request->all()); 	
			$opportunity = new Opportunity;

        	$opportunity->fill($request->only('title','description'));

			// if($request->hasFile('file')) {
			//     $image = ImageUploader::upload($request->file('file'), "uploads/opportunity");
			//     $opportunity->path = $image['path'];
			//     $opportunity->directory = $image['directory'];
			//     $opportunity->filename = $image['filename'];
			// }

			if($opportunity->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.opportunity.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {

			$this->data['page_title'] = " :: Opportunities - Edit record";
			$service = Opportunity::find($id);

			if (!$service) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return view('system.opportunity.index',$this->data);
			}

			$this->data['service'] = $service;
			return view('system.opportunity.edit',$this->data);
	}

	public function update (OpportunityRequest $request, $id = NULL) {
			try {
			$opportunity = Opportunity::find($id);


			// dd($request->all());

			if (!$opportunity) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.opportunity.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to update special record.");
				return redirect()->route('system.opportunity.index');	
			}
			$user = $request->user();


        	$opportunity->fill($request->only('title','description'));
        	

        

			if($opportunity->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.opportunity.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$about = Opportunity::find($id);


			if (!$about) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.opportunity.index');
			}

			if($about->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.opportunity.index');
			}

				session()->flash('notification-status','failed');
				session()->flash('notification-msg','Something went wrong.');

			} catch (Exception $e) {
				session()->flash('notification-status','failed');
				session()->flash('notification-msg',$e->getMessage());
				return redirect()->back();
			}
	}


}
