<?php

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\ContactInquiry;

/**
*
* Requests used for validating inputs
*/

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str,ImageUploader;

class ContactInquiryController extends Controller
{
   public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['heading'] = "Contacts";
	}

	public function index () {
		$this->data['page_title'] = " :: Contact Inquiries - Record Data";
		$this->data['contact_inquiry'] = ContactInquiry::orderBy('created_at',"DESC")->get();
		return view('system.inquiry.index',$this->data);
	}

	public function destroy ($id = NULL) {
		try {
			$contact_inquiry = ContactInquiry::find($id);


			if (!$contact_inquiry) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.inquiry.index');
			}

			if($contact_inquiry->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.inquiry.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}
