<?php 

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Faq;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\FaqRequest;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str;

class FaqController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	}

	public function index () {
		$this->data['page_title'] = " :: FAQ's - Record Data";
		$this->data['faqs'] = Faq::orderBy('updated_at',"DESC")->paginate(15);
		return view('system.faq.index',$this->data);
	}

	public function create () {
		$this->data['page_title'] = " :: FAQ's - Add new record";
		return view('system.faq.create',$this->data);
	}

	public function store (FaqRequest $request) {
		try {
			$new_faq = new Faq;

			$new_faq->fill($request->only('question', 'answer'));
			
			if($new_faq->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.faq.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$this->data['page_title'] = " :: FAQ's - Edit record";
		$faq = Faq::find($id);

		if (!$faq) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.faq.index');
		}

		if($id < 0){
			session()->flash('notification-status',"warning");
			session()->flash('notification-msg',"Unable to update special record.");
			return redirect()->route('system.faq.index');	
		}

		$this->data['faq'] = $faq;
		return view('system.faq.edit',$this->data);
	}

	public function update (FaqRequest $request, $id = NULL) {
		try {
			$faq = Faq::find($id);

			if (!$faq) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.faq.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to update special record.");
				return redirect()->route('system.faq.index');	
			}

			$faq->fill($request->only('question', 'answer'));

			if($faq->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.faq.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$faq = Faq::find($id);

			if (!$faq) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.faq.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to remove special record.");
				return redirect()->route('system.faq.index');	
			}

			if($faq->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.faq.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}