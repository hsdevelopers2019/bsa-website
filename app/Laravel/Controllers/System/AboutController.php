<?php namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/

use App\Laravel\Models\About;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\AboutRequest;


/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str,ImageUploader;

class AboutController extends Controller
{


	protected $data;


    public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	
		$this->data['heading'] = "Services";
	}

	public function index () {
		$this->data['page_title'] = "About us";
		$this->data['abouts'] = About::orderBy('created_at',"DESC")->get();
		return view('system.about.index',$this->data);
	}

	public function create () {


		
			$this->data['page_title'] = "Update About us";

			$this->data['about'] = About::get()->first();
			
			$this->data['description'] = About::get();
			return view('system.about.create',$this->data);
		

		
	}

	public function store (AboutRequest $request) {



		try {
			// dd($request->all()); 	
			$about = new About;

        	$about->fill($request->only('title','description'));

			if($request->hasFile('file')) {
			    $image = ImageUploader::upload($request->file('file'), "uploads/about");
			    $about->path = $image['path'];
			    $about->directory = $image['directory'];
			    $about->filename = $image['filename'];
			}

			if($about->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.about.create');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {

			$this->data['page_title'] = " :: About Us - Edit record";
			$about = About::find($id);

			if (!$about) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return view('system.about.index',$this->data);
			}

			$this->data['abouts'] = $about;
			return view('system.about.edit',$this->data);
	}

	public function update (AboutRequest $request) {

		// dd($request->all());
			try {
			$about = About::find($request->id);

			if (!$about) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.about.index');
			}

			if($request->id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to update special record.");
				return redirect()->route('system.about.index');	
			}
			$user = $request->user();
        	$about->fill($request->all());
        	// $about->excerpt = Helper::get_excerpt($request->get('content'));

        	if($request->hasFile('file')) {
        	    $image = ImageUploader::upload($request->file('file'), "uploads/images/abaout");
        	    $about->path = $image['path'];
        	    $about->directory = $image['directory'];
        	    $about->filename = $image['filename'];
            }

			if($about->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.about.create');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$about = About::find($id);


			if (!$about) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.about.index');
			}

			if($about->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.about.index');
			}

				session()->flash('notification-status','failed');
				session()->flash('notification-msg','Something went wrong.');

			} catch (Exception $e) {
				session()->flash('notification-status','failed');
				session()->flash('notification-msg',$e->getMessage());
				return redirect()->back();
			}
	}


}
