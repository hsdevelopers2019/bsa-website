<?php 

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Gallery;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\GalleryRequest;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, ImageUploader;

class GalleryController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['heading'] = "Gallery";
	}

	public function index () {
		$this->data['page_title'] = " :: Gallery - Record Data";
		$this->data['images'] = Gallery::orderBy('updated_at',"DESC")->paginate(15);
		return view('system.gallery.index',$this->data);
	}

	public function create () {
		$this->data['page_title'] = " :: Gallery - Add new record";
		return view('system.gallery.create',$this->data);
	}

	public function store (GalleryRequest $request) {
		try {
			$new_image = new Gallery;

			if($request->hasFile('file')) {
			    $image = ImageUploader::upload($request->file('file'), "uploads/gallery");
			    $new_image->path = $image['path'];
			    $new_image->directory = $image['directory'];
			    $new_image->filename = $image['filename'];
			}

			if($new_image->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.gallery.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$this->data['page_title'] = " :: Gallery - Edit record";

		$image = Gallery::find($id);

		if (!$image) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.gallery.index');
		}

		if($id < 0){
			session()->flash('notification-status',"warning");
			session()->flash('notification-msg',"Unable to update special record.");
			return redirect()->route('system.gallery.index');	
		}

		$this->data['image'] = $image;
		return view('system.gallery.edit',$this->data);
	}

	public function update (GalleryRequest $request, $id = NULL) {
		// try {
		// 	$image = Gallery::find($id);

		// 	if (!$image) {
		// 		session()->flash('notification-status',"failed");
		// 		session()->flash('notification-msg',"Record not found.");
		// 		return redirect()->route('system.gallery.index');
		// 	}

		// 	if($id < 0){
		// 		session()->flash('notification-status',"warning");
		// 		session()->flash('notification-msg',"Unable to update special record.");
		// 		return redirect()->route('system.gallery.index');	
		// 	}

		// 	if($request->hasFile('file')) {
		// 	    $image = ImageUploader::upload($request->file('file'), "uploads/gallery");
		// 	    $image->path = $image['path'];
		// 	    $image->directory = $image['directory'];
		// 	    $image->filename = $image['filename'];
		// 	}
			
		// 	if($image->save()) {
		// 		session()->flash('notification-status','success');
		// 		session()->flash('notification-msg',"Record has been modified successfully.");
		// 		return redirect()->route('system.gallery.index');
		// 	}

		// 	session()->flash('notification-status','failed');
		// 	session()->flash('notification-msg','Something went wrong.');

		// } catch (Exception $e) {
		// 	session()->flash('notification-status','failed');
		// 	session()->flash('notification-msg',$e->getMessage());
		// 	return redirect()->back();
		// }
		try {
			$about = Gallery::find($id);

			if (!$about) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.gallery.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to update special record.");
				return redirect()->route('system.gallery.index');	
			}
			$user = $request->user();
        	// $about->fill($request->only('title','description'));
        

        	if($request->hasFile('file')) {
        	    $image = ImageUploader::upload($request->file('file'), "uploads/images/services");
        	    $about->path = $image['path'];
        	    $about->directory = $image['directory'];
        	    $about->filename = $image['filename'];
            }

			if($about->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.gallery.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$image = Gallery::find($id);

			if (!$image) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.gallery.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to remove special record.");
				return redirect()->route('system.gallery.index');	
			}

			if($image->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.gallery.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}