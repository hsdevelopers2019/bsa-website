<?php 

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Team;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\TeamRequest;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, ImageUploader;

class TeamController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['heading'] = "Teams";
	}

	public function index () {
		$this->data['page_title'] = " :: Teams - Record Data";
		$this->data['teams'] = Team::orderBy('updated_at',"DESC")->paginate(15);
		return view('system.teams.index',$this->data);
	}

	public function create () {
		$this->data['page_title'] = " :: Teams - Add new record";
		return view('system.teams.create',$this->data);
	}

	public function store (TeamRequest $request) {
		try {
			$new_team = new Team;
			$new_team->fill($request->only('name', 'position', 'description'));

			if($request->hasFile('file')) {
			    $image = ImageUploader::upload($request->file('file'), "uploads/teams");
			    $new_team->path = $image['path'];
			    $new_team->directory = $image['directory'];
			    $new_team->filename = $image['filename'];
			}

			if($new_team->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.team.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$this->data['page_title'] = " :: Teams - Edit record";
		$team = Team::find($id);

		if (!$team) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.team.index');
		}

		if($id < 0){
			session()->flash('notification-status',"warning");
			session()->flash('notification-msg',"Unable to update special record.");
			return redirect()->route('system.team.index');	
		}

		$this->data['team'] = $team;
		return view('system.teams.edit',$this->data);
	}

	public function update (TeamRequest $request, $id = NULL) {
		try {
			$team = Team::find($id);

			if (!$team) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.team.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to update special record.");
				return redirect()->route('system.team.index');	
			}

			$team->fill($request->only('name', 'position', 'description'));

			if($request->hasFile('file')) {
			    $image = ImageUploader::upload($request->file('file'), "uploads/teams");
			    $team->path = $image['path'];
			    $team->directory = $image['directory'];
			    $team->filename = $image['filename'];
			}
			
			if($team->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.team.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$team = Team::find($id);

			if (!$team) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.team.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to remove special record.");
				return redirect()->route('system.team.index');	
			}

			if($team->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.team.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}