<?php 

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\ArticleCategory as Category;
use App\Laravel\Models\News;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\ArticleRequest;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str,ImageUploader;

class NewsController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = [ "pending" => "Pending for Approval",'yes' => "Approve and Published","no" => "Disapprove"];
		$this->data['categories'] = ['--Choose Category--'] + Category::pluck('name','id')->toArray();
		$this->data['heading'] = "News";
	}

	public function index () {
		$this->data['page_title'] = " :: News - Record Data";
		$this->data['news'] = News::orderBy('updated_at',"DESC")->paginate(15);
		return view('system.news.index',$this->data);
	}

	public function pending () {
		$this->data['page_title'] = " :: News - Pending for Approval";
		$this->data['news'] = News::where('is_approved','pending')->orderBy('updated_at',"DESC")->paginate(15);
		return view('system.news.index',$this->data);
	}

	public function published () {
		$this->data['page_title'] = " :: News - Published Data";
		$this->data['news'] = News::where('is_approved','yes')->orderBy('updated_at',"DESC")->paginate(15);
		return view('system.news.index',$this->data);
	}

	public function create () {
		$this->data['page_title'] = " :: News - Add new record";
		return view('system.news.create',$this->data);
	}

	public function store (ArticleRequest $request) {
		try {
			$new_news = new News;
			$user = $request->user();
        	$new_news->fill($request->only('title','video_url','content','category_id'));

			if($request->hasFile('file')) {
			    $image = ImageUploader::upload($request->file('file'), "uploads/images/users/{$user->id}/news");
			    $new_news->path = $image['path'];
			    $new_news->directory = $image['directory'];
			    $new_news->filename = $image['filename'];
            	$new_news->source = $image['source'];

			}
			// $new_news->status = $request->get('status');
			$new_news->user_id = $request->user()->id;
			if($new_news->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.news.pending');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$this->data['page_title'] = " :: News - Edit record";
		$news = News::find($id);

		if (!$news) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.news.index');
		}

		if($id < 0){
			session()->flash('notification-status',"warning");
			session()->flash('notification-msg',"Unable to update special record.");
			return redirect()->route('system.news.index');	
		}

		$this->data['news'] = $news;
		return view('system.news.edit',$this->data);
	}

	public function update (ArticleRequest $request, $id = NULL) {
		try {
			$news = News::find($id);

			if (!$news) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.news.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to update special record.");
				return redirect()->route('system.news.index');	
			}
			$user = $request->user();
        	$news->fill($request->only('title','video_url','content','category_id'));
        	if($request->hasFile('file')) {
        	    $image = ImageUploader::upload($request->file('file'), "uploads/images/users/{$user->id}/Newss");
        	    $news->path = $image['path'];
        	    $news->directory = $image['directory'];
        	    $news->filename = $image['filename'];
            	$news->source = $image['source'];
        	}

        	switch(Str::lower($request->get('is_approved'))){
        		case 'yes':
        			$news->is_approved = "yes";
        			$news->status = "published";
        		break;

        		case 'pending':
        			$news->is_approved = "pending";
        			$news->status = "draft";

        		break;

        		default:
        			$news->is_approved = "no";
        			$news->status = "declined";
        	}

			if($news->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.news.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$news = News::find($id);

			if (!$news) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.news.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to remove special record.");
				return redirect()->route('system.news.index');	
			}

			if($news->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.news.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}