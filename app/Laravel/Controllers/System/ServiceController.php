<?php namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Service;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\ServiceRequest;


/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str,ImageUploader;

class ServiceController extends Controller
{


	protected $data;


    public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	
		$this->data['heading'] = "Services";
	}

	public function index () {
		$this->data['page_title'] = " :: Services - Record Data";
		$this->data['services'] = Service::orderBy('created_at',"DESC")->get();
		return view('system.services.index',$this->data);
	}

	public function create () {
		$this->data['page_title'] = " :: Services - Add Service";
		return view('system.services.create',$this->data);
	}

	public function store(ServiceRequest $request){

		try {
			//dd($request->all());
			$new_service = new Service;
			$new_service->fill($request->all());
			$new_service->excerpt = Helper::get_excerpt($request->get('content'));
			
			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'uploads/images/services');
				$new_service->path = $upload["path"];
				$new_service->directory = $upload["directory"];
				$new_service->filename = $upload["filename"];
			}
			

			if($new_service->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New Service has been added.");
				return redirect()->route('system.service.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}


	}

	public function edit ($id = NULL) {

			$this->data['page_title'] = " :: Services - Edit record";
			$service = Service::find($id);

			if (!$service) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return view('system.services.index',$this->data);
			}

			$this->data['service'] = $service;
			return view('system.services.edit',$this->data);
	}

	public function update (ServiceRequest $request, $id = NULL) {
			try {
			$service = Service::find($id);

			if (!$service) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.service.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to update special record.");
				return redirect()->route('system.service.index');	
			}
			$user = $request->user();
        	$service->fill($request->only('service_title','content'));
        	$service->excerpt = Helper::get_excerpt($request->get('content'));

        	if($request->hasFile('file')) {
        	    $image = ImageUploader::upload($request->file('file'), "uploads/images/services");
        	    $service->path = $image['path'];
        	    $service->directory = $image['directory'];
        	    $service->filename = $image['filename'];
            }

			if($service->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.service.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$service = Service::find($id);


			if (!$service) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.service.index');
			}

			if($service->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.service.index');
			}

				session()->flash('notification-status','failed');
				session()->flash('notification-msg','Something went wrong.');

			} catch (Exception $e) {
				session()->flash('notification-status','failed');
				session()->flash('notification-msg',$e->getMessage());
				return redirect()->back();
			}
	}


}
