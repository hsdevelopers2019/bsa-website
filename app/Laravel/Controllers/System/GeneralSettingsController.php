<?php 

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
// use App\Laravel\Models\ArticleCategory as Category;
use App\Laravel\Models\Setting;
/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\SettingRequest;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, ImageUploader, Input;

class GeneralSettingsController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['heading'] = "General Settings";
	}

	public function index () {
		$this->data['page_title'] = " :: General Settings - Record Data";
		 $this->data['setting'] = Setting::orderBy('updated_at',"DESC")->first() ? : new Setting;
		return view('system.general-setting.edit', $this->data);
	}

	public function store (SettingRequest $request) {
		$id = Input::get('id');
		try {
			if ($id == 0)
			{
				$new_setting = new Setting;
	        	$new_setting->fill($request->only('address','email','contact','geo_lat','geo_long', 'fb_link', 'twitter_link', 'instagram_link', 'youtube_link', 'recipient_email'));

				if($request->hasFile('file')) {
				    $image = ImageUploader::upload($request->file('file'), "uploads/setting");
				    $new_setting->path = $image['path'];
				    $new_setting->directory = $image['directory'];
				    $new_setting->filename = $image['filename'];
				}

				if($new_setting->save()) {
					session()->flash('notification-status','success');
					session()->flash('notification-msg',"New record has been added.");
					return redirect()->route('system.settings.index');
				}
				session()->flash('notification-status','failed');
				session()->flash('notification-msg','Something went wrong.');

				return redirect()->back();
		
			}else{
				$setting = Setting::find($id);

				if (!$setting) {
					session()->flash('notification-status',"failed");
					session()->flash('notification-msg',"Record not found.");
					return redirect()->route('system.settings.index');
				}

				if($id < 0){
					session()->flash('notification-status',"warning");
					session()->flash('notification-msg',"Unable to update special record.");
					return redirect()->route('system.settings.index');	
				}

	        	$setting->fill($request->only('address','email','contact','geo_lat','geo_long', 'fb_link', 'twitter_link', 'instagram_link', 'youtube_link', 'recipient_email'));

	        	if($request->hasFile('file')) {
	        	    $image = ImageUploader::upload($request->file('file'), "uploads/setting");
	        	    $setting->path = $image['path'];
	        	    $setting->directory = $image['directory'];
	        	    $setting->filename = $image['filename'];
	        	}

				if($setting->save()) {
					session()->flash('notification-status','success');
					session()->flash('notification-msg',"Record has been modified successfully.");
					return redirect()->route('system.settings.index');
				}

				session()->flash('notification-status','failed');
				session()->flash('notification-msg','Something went wrong.');
			}
			
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}