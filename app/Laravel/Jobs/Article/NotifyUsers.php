<?php

namespace App\Laravel\Jobs\Article;

use App\Laravel\Models\User;
use Illuminate\Bus\Queueable;
use App\Laravel\Models\Article;
use App\Laravel\Models\MentorshipParticipant;
use App\Laravel\Models\Mentorship;


use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Laravel\Notifications\Article\NewArticleNotification;

class NotifyUsers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $article;

    /**
     * Create a new job instance.
     *
     * @param  User  $user
     * @param  Article  $article
     * @return void
     */
    public function __construct(User $user, Article $article)
    {
        $this->user = $user;
        $this->article = $article;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // $chat = Chat::find($this->chat->id);

        // $participants = MentorshipParticipant::where('user_id','')
        $mentorships = Mentorship::where('mentor_user_id',$this->user->id)->pluck('id')->toArray();

        if(count($mentorships) > 0){
            $implode_mentorships = implode(",", $mentorships);
            
            $participants = MentorshipParticipant::whereRaw("mentorship_id IN ($implode_mentorships)")->where('user_id','<>',$this->user->id)->pluck('user_id')->toArray();
            
            if(count($participants) > 0){
                $implode_users = implode(",", $participants);
                $users = User::whereRaw("id IN ($implode_users)")->get();
                foreach ($users as $key => $user) {
                    $user->notify(new NewArticleNotification($this->user,$this->article));
                }
            }
            
        }

        
    }
}