<?php 

namespace App\Laravel\Transformers;

use Input;
use JWTAuth, Carbon, Helper;
use App\Laravel\Models\User;
use App\Laravel\Models\Specialty;
use App\Laravel\Models\Mentorship;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Laravel\Transformers\MasterTransformer;

use Str;

class UserTransformer extends TransformerAbstract{

	protected $user,$auth;

	protected $availableIncludes = [
        'info', 'statistics',
    ];

    public function __construct() {
    	$this->auth = Auth::user();
    }

	public function transform(User $user) {

		$is_online = (strtotime(Carbon::now()) - strtotime($user->last_activity) ) <= 60 ? 1 : 0;
		
		if($is_online AND $user->last_activity != NULL){
			$is_online = true;
			$online_description = "Online";
		}else{
			$is_online = false;
			$online_description = $user->last_activity == NULL ? "Active ".$user->online_time_passed($user->created_at) : "Active ".$user->online_time_passed($user->last_activity);
		}

		$this->user = $user;

		$specialties = strlen($user->specialties) != "0" ? $user->specialties : "0";
		$user_specialties = Specialty::whereRaw("id IN ($specialties)")->pluck("name")->toArray();

		if($user->type == "mentor"){
			$num_mentorship = Mentorship::where('mentor_user_id',$user->id)->count();
		}else{
			$num_mentorship = Mentorship::where('mentee_user_id',$user->id)->count();
		}

	    return [
	     	'id' => $user->id ?:0,
	     	'is_online'	=> $is_online,
	     	'online_description'	=> $online_description,
	     	'name' => $user->name,
	     	'username' => $user->username,
	     	'type' => $user->type,
	     	'specialties' => $user->specialties, //1,2,3
	     	'specialty_id' => $user->specialties, //1,2,3
	     	'specialty' => count($user_specialties) > 0 ? implode(", ", $user_specialties) :"n/a",
	     	'num_specialty'	=> count($user_specialties),
	     	'num_mentorship' => $num_mentorship,
	     	'rating'	=> $user->rating,
	     	'num_rating'	=> $user->num_rating,
			'email' => $user->email,
			'description' => $user->description,
			'is_verified' => $user->is_verified,
			'address1' => $user->address1,
			'address2' => $user->address2,
			'city' => $user->city,
			'state' => $user->state,
			'country_iso' => $user->country_iso,
			'country_code' => $user->country_code,
			'contact_number' => $user->contact_number,
			'full_contact_number' => $user->country_code.$user->contact_number,
			'login_type' => ($user->fb_id OR $user->google_id)?"social_media":"email",
	     ];
	}

	public function includeInfo(User $user) {
		if($user->filename){
			$full_path = "{$user->new_directory}/resized/{$user->filename}";
			$thumb_path = "{$user->new_directory}/thumbnails/{$user->filename}";
		}else{
			$full_path = $thumb_path = asset('placeholder/user.jpg');
		}

		$collection = Collection::make([
			'member_since' => [
				'date_db' => $user->date_db($user->created_at,env("MASTER_DB_DRIVER","mysql")),
				'month_year' => $user->month_year($user->created_at),
				'time_passed' => $user->time_passed($user->created_at),
				'timestamp' => $user->created_at
			],
			'last_activity' => [
				'date_db' => $user->date_db($user->last_activity,env("MASTER_DB_DRIVER","mysql")),
				'month_year' => $user->month_year($user->last_activity),
				'time_passed' => $user->time_passed($user->last_activity),
				'timestamp' => $user->last_activity
			],
	     	'last_login' => [
				'date_db' => $user->date_db($user->last_login,env("MASTER_DB_DRIVER","mysql")),
				'month_year' => $user->month_year($user->last_login),
				'time_passed' => $user->time_passed($user->last_login),
				'timestamp' => $user->last_login
			],
			'avatar' => [
	 			'filename' => $user->filename,
	 			'path' => $user->path,
	 			'directory' => $user->new_directory,
	 			'full_path' => $full_path,
	 			'thumb_path' => $thumb_path,
			],
		]);
		return $this->item($collection, new MasterTransformer);
	}
}