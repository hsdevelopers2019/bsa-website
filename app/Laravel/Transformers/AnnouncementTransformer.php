<?php 

namespace App\Laravel\Transformers;

use Input;
use JWTAuth, Carbon, Helper,Str;
use App\Laravel\Models\Announcement;
use App\Laravel\Models\User;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Laravel\Transformers\MasterTransformer;



class AnnouncementTransformer extends TransformerAbstract{

	protected $availableIncludes = [
        'info','author'
    ];

	public function transform(Announcement $announcement) {

	    return [
	     	'id' => $announcement->id ?:0,
	     	'user_id' => $announcement->user_id,
	     	'title' => Str::title($announcement->title),
	     	'content' => $announcement->content,
	     	'excerpt'	=> $announcement->excerpt,
			'thumbnail' => [
	 			'filename' => $announcement->filename,
	 			'path' => $announcement->path,
	 			'directory' => $announcement->new_directory,
	 			'full_path' => "{$announcement->new_directory}/resized/{$announcement->filename}",
	 			'thumb_path' => "{$announcement->new_directory}/thumbnails/{$announcement->filename}",
			],
			'date_created' => [
				'date_db' => $announcement->date_db($announcement->created_at,env("MASTER_DB_DRIVER","mysql")),
				'month_year' => $announcement->month_year($announcement->created_at),
				'time_passed' => $announcement->time_passed($announcement->created_at),
				'timestamp' => $announcement->created_at
			],
	     ];
	}

	public function includeInfo(Announcement $announcement) {
		$collection = Collection::make([
			'date_created' => [
				'date_db' => $announcement->date_db($announcement->created_at,env("MASTER_DB_DRIVER","mysql")),
				'month_year' => $announcement->month_year($announcement->created_at),
				'time_passed' => $announcement->time_passed($announcement->created_at),
				'timestamp' => $announcement->created_at
			],
			
		]);
		return $this->item($collection, new MasterTransformer);
	}

	public function includeAuthor(Announcement $announcement){
       $user = $announcement->author ? : new User;
       if(is_null($user->id)){ $user->id = 0;}
       return $this->item($user, new UserTransformer);
	}
}