<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Laravel\Models\User;
use App\Laravel\Models\ActivityNotification;
use App\Laravel\Models\Article;
use App\Laravel\Models\ArticleComment;
use App\Laravel\Models\ArticleReaction;
use App\Laravel\Models\Chat;
use App\Laravel\Models\ChatConversation;
use App\Laravel\Models\ChatParticipant;
use App\Laravel\Models\Mentorship;
use App\Laravel\Models\MentorshipConversation;
use App\Laravel\Models\MentorshipParticipant;
use App\Laravel\Models\UserDevice;
use App\Laravel\Models\UserSocial;



class FullReset extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        ActivityNotification::truncate();
        Article::truncate();
        ArticleComment::truncate();
        ArticleReaction::truncate();
        Chat::truncate();
        ChatConversation::truncate();
        ChatParticipant::truncate();
        Mentorship::truncate();
        MentorshipConversation::truncate();
        MentorshipParticipant::truncate();
        UserDevice::truncate();
        UserSocial::truncate();

        $this->call(AdminAccountSeeder::class);

    }
}
