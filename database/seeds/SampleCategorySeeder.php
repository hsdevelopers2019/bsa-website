<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Laravel\Models\ArticleCategory;

class SampleCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $categories = ['Starting a Business','Negosyo Opportunities'];

        foreach($categories as $index => $category){
            $_category = strtolower($category);
            $new_category = ArticleCategory::whereRaw("LOWER(name) = '{$_category}'")->first();

            if(!$new_category){
                $new_category = new ArticleCategory;
                $new_category->user_id = 1;
                $new_category->name = $category;
            }
            $new_category->status = "active";
            $new_category->save();     
        }

    }
}
