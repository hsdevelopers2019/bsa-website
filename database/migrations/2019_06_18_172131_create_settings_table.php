<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('settings', function($table){
            $table->increments('id');
            $table->string('recipient_email', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('contact', 255)->nullable();
            $table->string('address', 255)->nullable();
            $table->string('geo_lat')->nullable();
            $table->string('geo_long')->nullable();
            $table->string('fb_link', 255)->nullable();
            $table->string('twitter_link', 255)->nullable();
            $table->string('instagram_link', 255)->nullable();
            $table->string('youtube_link', 255)->nullable();
            $table->text('path')->nullable();
            $table->text('directory')->nullable();
            $table->text('filename')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('settings');
    }
}
