<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIosVesionControlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ios_version_control', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->string('version_name');
            $table->string('major_version');
            $table->string('minor_version');
            $table->text('changelogs');
            $table->integer('maintenance_counter')->default(0);
            $table->enum('is_maintenance',['yes', 'no'])->default('no');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ios_version_control');
    }
}
