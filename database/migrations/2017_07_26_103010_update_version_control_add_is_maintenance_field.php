<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateVersionControlAddIsMaintenanceField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('version_control', function ($table) {
            $table->integer('maintenance_counter')->default(0)->after('minor_version');
            $table->enum('is_maintenance',['yes', 'no'])->default('no')->after('minor_version');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('version_control', function ($table) {
            $table->dropColumn(['is_maintenance', 'maintenance_counter']);
        });;
    }
}
