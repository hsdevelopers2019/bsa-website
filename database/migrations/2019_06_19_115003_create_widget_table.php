<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWidgetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('widget', function($table){
            $table->increments('id');
            $table->string('type')->nullable();
            $table->string('title', 255)->nullable();
            $table->longText('description')->nullable();
            $table->text('path')->nullable();
            $table->text('directory')->nullable();
            $table->text('filename')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('widget');
    }
}
