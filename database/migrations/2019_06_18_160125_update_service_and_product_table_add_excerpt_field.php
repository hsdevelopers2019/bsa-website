<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateServiceAndProductTableAddExcerptField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('services', function($table){
            $table->string('excerpt', 255)->nullable()->after('content');
            $table->softDeletes();
        });

        Schema::table('products', function($table){
            $table->string('excerpt', 255)->nullable()->after('content');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('services', function($table){
            $table->dropColumn(['excerpt']);
            $table->dropSoftDeletes();
        });

        Schema::table('products', function($table){
            $table->dropColumn(['excerpt']);
            $table->dropSoftDeletes();
        });
    }
}
