<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePageAndWidgetTableAddSlugField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('page', function($table){
            $table->string('slug', 255)->nullable();
        });

        Schema::table('widget', function($table){
            $table->string('slug', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('page', function($table){
            $table->dropColumn(['slug']);
        });

        Schema::table('widget', function($table){
            $table->dropColumn(['slug']);
        });
    }
}
